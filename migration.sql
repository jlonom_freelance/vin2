CREATE TABLE IF NOT EXISTS `groups` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`price` INT(11) NOT NULL,
	`day` INT(11) NOT NULL,
	`month` INT(11) NOT NULL,
	`created_at` TIMESTAMP NOT NULL,
	`updated_at` TIMESTAMP NOT NULL,
	PRIMARY KEY(`id`)
) COLLATE='utf8_general_ci';

CREATE TABLE IF NOT EXISTS `orders` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`total` INT(11) NOT NULL,
	`status_id` INTEGER NOT NULL,
	`user_id` INT(11) NOT NULL,
	`vin_id` INT(11) NOT NULL,
	`created_at` TIMESTAMP NOT NULL,
	`updated_at` TIMESTAMP NOT NULL,
	PRIMARY KEY(`id`)
) COLLATE='utf8_general_ci';

CREATE TABLE IF NOT EXISTS `payments` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`order_id` INT(11),
	`type` VARCHAR(255) NOT NULL,
	`data` TEXT,
	`created_at` TIMESTAMP NOT NULL,
	`updated_at` TIMESTAMP NOT NULL,
	PRIMARY KEY(`id`)
) COLLATE='utf8_general_ci';

CREATE TABLE IF NOT EXISTS `telegram` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`telegram_id` INT(11) NOT NULL,
	`user_id` INT(11) NOT NULL,
	`updated_at` TIMESTAMP NOT NULL,
	`created_at` TIMESTAMP NOT NULL,
	PRIMARY KEY(`id`)
) COLLATE='utf8_general_ci';

CREATE TABLE IF NOT EXISTS `transactions` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`order_id` INT(11) NOT NULL,
	`user_id` INT(11) NOT NULL,
	`token` VARCHAR(255) NOT NULL,
	`status` INT(11) NOT NULL,
	`value` DECIMAL(8,4) NOT NULL,
	`created_at` TIMESTAMP NOT NULL,
	`updated_at` TIMESTAMP NOT NULL,
	`created_by` INT(11),
	PRIMARY KEY(`id`)
) COLLATE='utf8_general_ci';

CREATE TABLE IF NOT EXISTS `users` (
	`id`	INT(11) NOT NULL AUTO_INCREMENT,
	`group_id`	INT(11) NOT NULL,
	`name`	VARCHAR(255) NOT NULL,
	`email`	VARCHAR(255) NOT NULL,
	`password`	VARCHAR(255) NOT NULL,
	`status`	INT(11) NOT NULL,
	`created_at`	TIMESTAMP NOT NULL,
	`updated_at`	TIMESTAMP NOT NULL,
	`token`	VARCHAR(255) NOT NULL,
	PRIMARY KEY(`id`)
) COLLATE='utf8_general_ci';

CREATE TABLE IF NOT EXISTS `vin_request` (
	`id`	INTEGER NOT NULL AUTO_INCREMENT,
	`status`	INT(11) NOT NULL,
	`text`	VARCHAR(255) NOT NULL,
	`filename`	TEXT,
	`created_by`	INT(11) NOT NULL,
	`created_at`	TIMESTAMP NOT NULL,
	`updated_at`	TIMESTAMP NOT NULL,
	PRIMARY KEY(`id`)
) COLLATE='utf8_general_ci';