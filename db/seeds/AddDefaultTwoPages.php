<?php


use Phinx\Seed\AbstractSeed;

class AddDefaultTwoPages extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Пользовательское соглашение',
                'content' => <<<CONTENT
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus bibendum, orci in tristique pretium, est mi feugiat metus,
non fringilla risus diam eu tortor. Integer ullamcorper sem vitae erat varius, eget dapibus dolor scelerisque.
Vestibulum augue arcu, euismod a nibh vel, condimentum pharetra lorem. Nam ornare justo tincidunt commodo tincidunt.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec eu massa nulla.
Nullam ultrices lacinia commodo. Suspendisse tincidunt arcu at diam varius lacinia. Donec suscipit sem ac tempor pellentesque.
Curabitur posuere venenatis libero vel rutrum. Nam tempor dapibus aliquam. Donec lacinia mauris dui,
et pulvinar lorem accumsan at. Fusce volutpat velit eu facilisis posuere. Ut commodo ut odio at elementum. Donec ut magna erat.
CONTENT
                ,
                'slug' => 'terms',
                'seo_title' => 'Пользовательское соглашение',
                'seo_description' => 'Пользовательское соглашение',
            ],
            [
                'title' => 'Условия возврата',
                'content' => <<<CONTENT
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus sed suscipit turpis,
sit amet molestie mi. Ut non congue dolor. Mauris vitae est nec turpis lobortis pharetra. Pellentesque ac magna viverra,
luctus tellus in, iaculis sapien. Nunc fringilla, nunc nec hendrerit rhoncus, felis nisl condimentum diam, at tristique
nisl neque eu sapien. Nulla pretium finibus nisi, quis ornare felis suscipit non. Donec porta, metus vitae gravida
hendrerit, risus dolor elementum sem, vel finibus nibh lectus nec orci. Sed a pharetra urna. Aliquam efficitur bibendum
enim nec egestas. Vestibulum feugiat sem quis faucibus venenatis. Donec consectetur viverra sem. Suspendisse pharetra
pharetra mauris, a scelerisque est.
CONTENT
                ,
                'slug' => 'conditions',
                'seo_title' => 'Условия возврата',
                'seo_description' => 'Условия возврата',
            ],
        ];

        $table = $this->table('pages');
        $table->insert($data)->saveData();
    }
}
