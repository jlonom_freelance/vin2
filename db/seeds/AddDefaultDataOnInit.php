<?php


use Phinx\Seed\AbstractSeed;

class AddDefaultDataOnInit extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id' => env('ADMIN_GROUP_ID'),
                'name' => 'Admin group',
                'price' => 0,
                'day' => date('d'),
                'month' => date('m'),
            ],
            [
                'id' => env('DEFAULT_GROUP_ID'),
                'name' => 'Default group',
                'price' => 100,
                'day' => date('d'),
                'month' => date('m'),
            ]
        ];

        $table = $this->table('groups');
        $table->insert($data)->saveData();
    }
}
