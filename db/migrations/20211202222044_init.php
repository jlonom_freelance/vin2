<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Init extends AbstractMigration
{
    public function up(): void
    {
        if (!$this->hasTable('groups')) {
            $table = $this->table('groups');
            $table->addColumn('name', 'string', ['limit' => 255])
                ->addColumn('price', 'integer')
                ->addColumn('day', 'integer')
                ->addColumn('month', 'integer')
                ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->hasTable('orders')) {
            $table = $this->table('orders');
            $table->addColumn('total', 'integer')
                ->addColumn('status_id', 'integer')
                ->addColumn('user_id', 'integer')
                ->addColumn('vin_id', 'integer')
                ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->hasTable('payments')) {
            $table = $this->table('payments');
            $table->addColumn('order_id', 'integer')
                ->addColumn('type', 'string', ['limit' => 255])
                ->addColumn('data', 'text')
                ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->hasTable('telegram')) {
            $table = $this->table('telegram');
            $table->addColumn('name', 'string', ['limit' => 255])
                ->addColumn('telegram_id', 'integer')
                ->addColumn('user_id', 'integer')
                ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->hasTable('transactions')) {
            $table = $this->table('transactions');
            $table->addColumn('order_id', 'integer')
                ->addColumn('user_id', 'integer')
                ->addColumn('token', 'string', ['limit' => 255])
                ->addColumn('status', 'integer')
                ->addColumn('value', 'decimal', ['precision' => 8, 'scale' => 4])
                ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
                ->addColumn('created_by', 'integer')
                ->create();
        }

        if (!$this->hasTable('users')) {
            $table = $this->table('users');
            $table->addColumn('group_id', 'integer', ['null' => true])
                ->addColumn('name', 'string', ['limit' => 255])
                ->addColumn('email', 'string', ['limit' => 255])
                ->addColumn('telephone', 'string', ['limit' => 255])
                ->addColumn('password', 'string', ['limit' => 255])
                ->addColumn('status', 'integer')
                ->addColumn('email_verified_at', 'timestamp', ['null' => true])
                ->addColumn('token', 'string', ['limit' => 255, 'null' => true])
                ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->hasTable('vin_request')) {
            $table = $this->table('vin_request');
            $table->addColumn('status', 'integer')
                ->addColumn('text', 'string', ['limit' => 255])
                ->addColumn('filename', 'text')
                ->addColumn('created_by', 'integer')
                ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
                ->create();
        }
    }

    public function down()
    {
        $this->table('vin_request')->drop()->save();
        $this->table('users')->drop()->save();
        $this->table('transactions')->drop()->save();
        $this->table('telegram')->drop()->save();
        $this->table('payments')->drop()->save();
        $this->table('orders')->drop()->save();
        $this->table('groups')->drop()->save();
    }
}
