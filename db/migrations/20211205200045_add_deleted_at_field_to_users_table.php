<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddDeletedAtFieldToUsersTable extends AbstractMigration
{
    public function up()
    {
        $this->table('users')->addColumn('deleted_at', 'timestamp', ['null' => true])->save();
    }

    public function down()
    {
        $this->table('users')->removeColumn('deleted_at')->save();
    }
}
