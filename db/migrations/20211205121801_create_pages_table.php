<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreatePagesTable extends AbstractMigration
{
    public function up()
    {
        if (!$this->hasTable('pages')) {
            $table = $this->table('pages');

            $table->addColumn('title', 'string', ['limit' => 255])
                ->addColumn('content', 'text')
                ->addColumn('slug', 'string', ['limit' => 255])
                ->addColumn('seo_title', 'string', ['limit' => 255])
                ->addColumn('seo_description', 'string', ['limit' => 255])
                ->addColumn('created_by', 'integer', ['null' => true])
                ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
                ->create();
        }
    }

    public function down()
    {
        if ($this->hasTable('pages')) {
            $this->table('pages')->drop()->save();
        }
    }
}
