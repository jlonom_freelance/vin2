# Installation

1)
```shell
cp .env.dist .env
```

2) Edit .env with your parameters


3)
```shell
composer install
```

4)
```shell
vendor/bin/phinx migrate
```

5)
```shell
vendor/bin/phinx seed:run
```