<?php

namespace App\Extensions;

use Slim\Views\TwigExtension as BaseExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends BaseExtension
{
    public function getFunctions()
    {
        $functions = parent::getFunctions();
        $functions[] = new \Twig\TwigFunction('get_env', array($this, 'getEnv'));
        $functions[] = new TwigFunction('html_entity_decode','html_entity_decode', ['is_safe' => ['html']]);
        return $functions;
    }

    public function getEnv($key)
    {
        return slim_env($key);
    }
}