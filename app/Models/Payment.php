<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {
   protected $table = 'payments';

   protected $fillable = ['type','data'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}