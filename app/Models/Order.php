<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {
   protected $table = 'orders';

    protected $attributes = [
   	    'status_id' => 1,
    ];

    protected $fillable = ['total', 'status_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function getStatusAttribute() {
    	switch ($this->status_id) {
    		case 1:
    			return 'Ожидание оплаты';
    			break;
    		case 2:
    			return 'В обработке';
    			break;
    		case 3:
    			return 'Успешно';
    			break;
            case 4:
                return 'Отменено';
                break;
    		default:
    			return 'Неизвестно';
    			break;
    	}
    }

    public function vin() {
        return $this->belongsTo(VinRequest::class, 'vin_id');
    }

    public function transactions() {
        return $this->hasMany(Transaction::class);
    }
}