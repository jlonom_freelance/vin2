<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telegram extends Model {
    protected $table = 'telegram';

    protected $fillable = ['name','telegram_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}