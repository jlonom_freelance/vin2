<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $status
 * @property string $text
 * @property string $filename
 */
class VinRequest extends Model {
   	protected $table = 'vin_request';

   	protected $attributes = [
   		'status' => 0,
   		'filename' => '',
   	];

   	protected $fillable = ['text', 'status', 'filename'];

    public function creator() {
        return $this->belongsTo(User::class, 'created_by');
    }
    
}