<?php
namespace App\Models;

use App\Services\EmailService;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class User extends Model {
    protected $table = 'users';

    protected $attributes = [
        'telephone' => '',
   		'token' => '',
        'status' => 1,
   	];

    protected $fillable = [
        'name',
        'email',
        'group_id',
        'password',
        'telephone',
        'token',
        'deleted_at',
        'email_verified_at'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function telegram()
    {
        return $this->hasOne(Telegram::class);
    }

    public function orders()
    {
    	return $this->hasMany(Order::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function getBalanceAttribute()
    {
        return $this->transactions()->where('status', '=', 1)->sum('value');
    }

    public static function findByToken($token): User
    {
        return self::query()->where('token', $token)->firstOrFail();
    }

    public function delete()
    {
        return $this->update([
            'deleted_at' => date('Y-m-d H:i:s'),
            'group_id' => null,
        ]);
    }
}