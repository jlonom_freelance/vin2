<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {
   protected $table = 'groups';

   protected $attributes = [
        'month' => 0,
        'day' => 0,
   	];

   protected $fillable = ['name','price','month','day'];

   public function users()
    {
        return $this->hasMany(User::class);
    }
}