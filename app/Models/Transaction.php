<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {
   	protected $table = 'transactions';

   	protected $attributes = [
        'status' => 0,
        'order_id' => 0,
        'user_id' => 0,
        'created_by' => 0,
        'token' => '',
   	];

   	protected $fillable = ['token', 'value', 'status'];

    public function creator() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}