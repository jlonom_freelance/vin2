<?php

namespace App\Http\Controllers;

use App\Helpers\Google\ReCaptcha\Client;
use App\Http\Controller;
use App\Models\User;
use App\Models\Group;
use App\Services\EmailService;
use libphonenumber\PhoneNumberUtil;
use Slim\Http\Request;
use Slim\Http\Response;
use RuntimeException;

/**
 * @property Client $recaptcha
 */
class AuthController extends Controller
{
	private $error = array();

	public function login(Request $request, Response $response, array $args) {
		if($request->isPost() && $this->validateLogin($request)) {
			return $response->withRedirect($this->router->pathFor('home'));
		}

	    return $this->view->render($response, 'auth/login.twig', [
			'email' => $request->getParsedBodyParam('email'),
			'password' => $request->getParsedBodyParam('password'),
			'error' => $this->error,
            'publicReCaptchaKey' => env('GOOGLE_CAPTCHA_V2_PUBLIC')
		]);
	}

	public function info(Request $request, Response $response, array $args) {
		$user = $this->auth->user();

		$balance = 0;
		$telegram_name = '';

		$telegram = $user->telegram;

		if($telegram){
			$telegram_name = $telegram->name;
		}

		$transactions = $user->transactions()->orderBy('created_at', 'DESC')->paginate(10);

	    return $this->view->render($response, 'auth/info.twig', [
			'balance' => $balance,
			'name' => $user->name,
			'telephone' => $user->telephone,
			'email' => $user->email,
			'telegram' => $telegram_name,
			'transactions' => $transactions,
		]);
	}

	public function logout(Request $request, Response $response, array $args) {
		$this->auth->logout();

		return $response->withRedirect($this->router->pathFor('home'));
	}

	public function edit(Request $request, Response $response, array $args) {
		$user = $this->auth->user();

		if($request->isPost() && $this->validateEdit($request)){
			$user->update([
				'name' => $request->getParsedBodyParam('name'), 
				'email' => $request->getParsedBodyParam('email'), 
				'telephone' => $request->getParsedBodyParam('telephone'),
			]);

			if($request->getParsedBodyParam('password')){
				$hash = password_hash($request->getParsedBodyParam('password'), PASSWORD_BCRYPT, [
		            'cost' => 10,
		        ]);

				$user->update([
					'password' => $hash,
				]);
			}

			return $response->withRedirect($this->router->pathFor('auth.info'));
		}

		return $this->view->render($response, 'auth/edit.twig', [
			'is_telegram' => $user->telegram ? true : false,
			'name' => $request->getParsedBodyParam('name', $user->name),
			'email' => $request->getParsedBodyParam('email', $user->email),
			'telephone' => $request->getParsedBodyParam('telephone', $user->telephone),
			'password' => $request->getParsedBodyParam('password'),
			'confirm' => $request->getParsedBodyParam('confirm'),
			'error' => $this->error,
		]);
	}

	public function telegram(Request $request, Response $response, array $args) {
		$user = $this->auth->user();
		$settings = $this->settings;

		$user->update([
			'token' => token(32),  
		]);

		$client = new \GuzzleHttp\Client([
		    'base_uri' => 'https://api.telegram.org',
		    'timeout'  => 10.0,
		]);

		$telegram_response = $client->request('GET', '/bot' . $settings['telegram']['key'] . '/getMe');

		$telegram_data = json_decode($telegram_response->getBody(), true);

		if(!empty($telegram_data['result']['username'])) {
			$url = 'https://t.me/' . $telegram_data['result']['username'] . '?start=' . $user->token;

			return $response->withRedirect($url);
		}

		return $response->withRedirect($this->router->pathFor('auth.edit'));
	}

	public function telegramDisconnect(Request $request, Response $response, array $args) {
		$user = $this->auth->user();

		$user->telegram->delete();
		
		return $response->withRedirect($this->router->pathFor('auth.edit'));
	}

	public function register(Request $request, Response $response, array $args) {
		if($request->isPost() && $this->validateRegister($request)){
			$hash = password_hash($request->getParsedBodyParam('password'), PASSWORD_BCRYPT, [
	            'cost' => 10,
	        ]);
            $group = Group::find(slim_env('DEFAULT_GROUP_ID'));

			$user = new User([
				'status' => 1,
				'group_id' => $group?->id,
				'name' => $request->getParsedBodyParam('name'),
				'email' => utf8_strtolower($request->getParsedBodyParam('email')),
				'telephone' => $request->getParsedBodyParam('telephone'),
				'password' => $hash,
                'token' => token(64)
			]);

			$user->save();

			$mailService = new EmailService();
			$mailHtml = $this->view->fetch('mail/confirm_email.twig', [
			    'tokenUrl' => $this->router->pathFor('user.confirmEmail', ['token' => $user->token]),
                'siteName' => env('APP_NAME'),
            ]);

			if ($mailService->sendMail($user->email, 'Подтверждение электронной почты', $mailHtml)) {
                return $this->view->render($response, 'auth/register_success.twig');
            } else {
			    throw new RuntimeException("Не получилось отправить сообщение: " . $mailService->getError());
            }
		}

		return $this->view->render($response, 'auth/register.twig', [
			'name' => $request->getParsedBodyParam('name'),
			'email' => $request->getParsedBodyParam('email'),
			'telephone' => $request->getParsedBodyParam('telephone'),
			'password' => $request->getParsedBodyParam('password'),
			'confirm' => $request->getParsedBodyParam('confirm'),
			'error' => $this->error,
		]);
	}


	protected function validateLogin($request){
        if($this->recaptcha->getPublic()
            && !$this->recaptcha->verify($request->getParsedBodyParam('g-recaptcha-response', ''))
        ){
            $this->error['captcha'] = 'Нужно выполнить капчу';
        }

		if(!$this->auth->login($request)) {
			$this->error['login'] = 'Некорректные данные для входа';
		}

		return !$this->error;
	}

	protected function validateRegister(Request $request){
		if(
		    $this->recaptcha->getPublic()
            && !$this->recaptcha->verify($request->getParsedBodyParam('g-recaptcha-response', ''))
        ){
			$this->error['captcha'] = 'Нужно выполнить капчу';
		}

		if (
		    (utf8_strlen(trim($request->getParsedBodyParam('name'))) < 1)
            || (utf8_strlen(trim($request->getParsedBodyParam('name'))) > 32)
        ) {
			$this->error['name'] = 'Name must be from 4 to 32 symbol';
		}

		if (
		    (utf8_strlen($request->getParsedBodyParam('email')) > 96)
            || !filter_var($request->getParsedBodyParam('email'), FILTER_VALIDATE_EMAIL)
        ) {
			$this->error['email'] = 'Email must be valid';
		}

		$user = User::where('email', '=', utf8_strtolower($request->getParsedBodyParam('email')))->first();

		if($user){
			$this->error['email'] = 'Email exists';
		}

		if (
		    (utf8_strlen(html_entity_decode($request->getParsedBodyParam('password'), ENT_QUOTES, 'UTF-8')) < 4)
            || (utf8_strlen(html_entity_decode($request->getParsedBodyParam('password'), ENT_QUOTES, 'UTF-8')) > 40)
        ) {
			$this->error['password'] = 'Password must be from 4 to 40 symbol';
		}

		if($request->getParsedBodyParam('password')) {
			if ($request->getParsedBodyParam('password') != $request->getParsedBodyParam('confirm')) {
				$this->error['confirm'] = 'Confirm must be valid';
			}
		}

		if (($phone = $request->getParsedBodyParam('telephone')) !== null) {
            $phoneNumberUtil = PhoneNumberUtil::getInstance();
            $phoneNumber = $phoneNumberUtil->parse($phone, 'UA');
            if (!$phoneNumberUtil->isValidNumber($phoneNumber)) {
                $this->error['telephone'] = 'Invalid phone number';
            }
        } else {
            $this->error['telephone'] = 'Confirm must be valid';
        }

		return !$this->error;
	}

	protected function validateEdit($request){
		if ((utf8_strlen(trim($request->getParsedBodyParam('name'))) < 1) || (utf8_strlen(trim($request->getParsedBodyParam('name'))) > 32)) {
			$this->error['name'] = 'Name must be from 4 to 32 symbol';
		}

		if ((utf8_strlen($request->getParsedBodyParam('email')) > 96) || !filter_var($request->getParsedBodyParam('email'), FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = 'Email must be valid';
		}

		if($this->auth->getEmail() != $request->getParsedBodyParam('email')){
			$user = User::where('email', '=', utf8_strtolower($request->getParsedBodyParam('email')))->first();

			if($user){
				$this->error['email'] = 'Email exists';
			}
		}
		
		if($request->getParsedBodyParam('password')) {
			if ((utf8_strlen(html_entity_decode($request->getParsedBodyParam('password'), ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($request->getParsedBodyParam('password'), ENT_QUOTES, 'UTF-8')) > 40)) {
				$this->error['password'] = 'Password must be from 4 to 40 symbol';
			}
		
			if ($request->getParsedBodyParam('password') != $request->getParsedBodyParam('confirm')) {
				$this->error['confirm'] = 'Confirm must be valid';
			}
		}	

		return !$this->error;
	}
}