<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\User;
use App\Models\Telegram;

class TelegramController extends Controller
{
	private $error = array();

	public function webhook(Request $request, Response $response, array $args) {
		$message = $request->getParsedBodyParam('message', []);

		if(!empty($message['text']) && utf8_substr($message['text'], 0, 6) == '/start') {
			$parts = explode(' ', $message['text']);

			if(!empty($parts[1])) {
				$token = $parts[1]; 

				$user = User::where('token', '=', $token)->first();

				if($user){
					if($user->telegram){
						$user->telegram->delete();
					}

					$telegram = new Telegram([
						'name' => $message['from']['username'],
						'telegram_id' => $message['from']['id'], 
					]);
				
					$telegram->user()->associate($user);

					$telegram->save();
				}
			}
		} 

		return $response; 
	}

	public function download(Request $request, Response $response, array $args) {
		$settings = $this->settings;
		$token = $args['token'];

		$output_name = $request->getQueryParam('filename');

		if(!$output_name){
			$output_name = 'report';
		}

		$filename = $settings['vin_api']['path'] . $token . '.blob';

		if(file_exists($filename) && is_file($filename)) {
			if($resource = fopen($filename, "r")) {
	            $size = filesize($filename);

	            $output_name = $output_name . '.pdf';

	            $response = $response->withHeader("Content-type","application/pdf");
	            $response = $response->withHeader("Content-Disposition", 'attachment;filename="' . $output_name . '"');
	            $response = $response->withHeader("Cache-control", "private");
	            $response = $response->withHeader("Content-length", $size);
	        }

	        $stream = new \Slim\Http\Stream($resource);

			return $response->withBody($stream);
		}

		return $response; 
	}

	public function setWebhook(Request $request, Response $response, array $args) {
		$settings = $this->settings;
		$router = $this->router;

		$uri = \Slim\Http\Uri::createFromString($settings['base_path']);

        $callback_url = $router->fullUrlFor($uri, 'telegram.webhook', [], []);

		$client = new \GuzzleHttp\Client([
		    'base_uri' => 'https://api.telegram.org',
		    'timeout'  => 10.0,
		]);

    	$guzzle_response = $client->request('POST', '/bot' . $settings['telegram']['key'] . '/setWebhook', [
        	'form_params' => [
        		'url' => $callback_url,
        	],
		]);

		return $response; 
	}

	public function deleteWebhook(Request $request, Response $response, array $args) {
		$client = new \GuzzleHttp\Client([
		    'base_uri' => 'https://api.telegram.org',
		    'timeout'  => 10.0,
		]);

    	$guzzle_response = $client->request('POST', '/bot' . $settings['telegram']['key'] . '/deleteWebhook', [
        	'form_params' => [],
		]);

		return $response; 
	}
}