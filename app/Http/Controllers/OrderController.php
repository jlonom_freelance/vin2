<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use App\Services\VinCheckApiService;
use DateTime;
use GuzzleHttp\Client;
use Slim\Exception\NotFoundException;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\User;
use App\Models\Group;
use App\Models\Order;
use App\Models\VinRequest;
use App\Models\Transaction;
use Throwable;

/**
 * Class OrderController
 *
 * @package App\Http\Controllers
 * @property \App\Helpers\Olshansky\Auth\Driver $auth
 */
class OrderController extends Controller
{
	private $error = array();

	/*
		status_id

	    1 - Waiting for payment,
	    2 - Processing,
	    3 - Order success,
	    4 - Order canceled,
	    other - Unknown,
	*/

	public function download(Request $request, Response $response, array $args) {
		$settings = $this->settings;
        $router = $this->router;
		$order = Order::findOrFail($args['id']);

		if($order->vin->status != 1 || ($this->auth->user->group->id != 8 && $this->auth->getId() != $order->user->id)) {
			return $response->withRedirect($this->router->pathFor('order.list'));
		}

		$filename = $settings['vin_api']['path'] . $order->vin->filename;

        if (!file_exists($filename) || !is_file($filename)) {
            $service = new VinCheckApiService($settings->all(), $router, $this->logger, false);
            $file = $service->getData($order->vin);
            if ($file !== false) {
                $filename = $file;
            }
        }

		if(file_exists($filename) && is_file($filename)) {
			if($resource = fopen($filename, "r")) {
	            $size = filesize($filename);
	            $ext = 'pdf';

	            $output_name = $order->vin->text . '.' . $ext;
	            
	            switch ($ext) {
	                case "pdf":
	                    $response = $response->withHeader("Content-type","application/pdf");
	                    break;
	                default;
	                    $response = $response->withHeader("Content-type","application/octet-stream");
	                    break;
	            }

	            $response = $response->withHeader("Content-Disposition", 'attachment;filename="' . $output_name . '"');
	            $response = $response->withHeader("Cache-control", "private");
	            $response = $response->withHeader("Content-length", $size);
	        }

	        $stream = new \Slim\Http\Stream($resource);

			return $response->withBody($stream);
        }

       	throw new NotFoundException($request, $response);
	}

	public function queue(Request $request, Response $response, array $args) {
		$settings = $this->settings;
		$router = $this->router;

		$orders = Order::where('status_id', '=', 2)->get();

		$queue = [];

		foreach($orders as $order){
			if($order->vin()->exists() && !$order->vin->status){
				$queue[] = $order->vin;
			}
		}

		$queue = array_unique($queue);

		foreach($queue as $vin){
            $service = new VinCheckApiService($settings->all(), $router, true);
            $service->getData($vin);
		}

		return $response->withJson(['data' => 'data']);
	}

    public function webhook(Request $request, Response $response, array $args) {
    	$settings = $this->settings;
    	$router = $this->router;

    	if($request_id = $request->getQueryParam('request_id')) {
    		$vin = VinRequest::find($request_id);
    	} elseif($request_vin = $request->getQueryParam('vin')) {
    		$vin = VinRequest::where('text', '=', utf8_strtoupper($request->getQueryParam('vin')))->first();
    	}

    	if($vin && !$vin->status) {
    	    $this->logger->info('[Have response]', ['request' => $request->getQueryParams()]);
            $service = new VinCheckApiService($settings->all(), $router, true);
            $service->getData($vin);
        }

        return $response;
	}

	public function create(Request $request, Response $response, array $args) {
		$price = 0;
		$quota = $this->auth->getQuota();

		if(!$quota){
			$price = $this->auth->getPrice();
		}

		$settings = $this->settings;
		$router = $this->router;

		if($request->isPost() && $this->validate($request)) {
			$vin = VinRequest::where('text', '=', utf8_strtoupper($request->getParsedBodyParam('vin')))->whereIn('status', [0, 1])->whereDate('created_at', '>', \Carbon\Carbon::now()->subDays(15))->orderBy('created_at', 'DESC')->first();

			if(!$vin){
				$vin = new VinRequest([
					'text' => utf8_strtoupper($request->getParsedBodyParam('vin')),
				]);

				$vin->creator()->associate($this->auth->user);
				$vin->save();
			}

			$order = new Order([
				'total' => $price,
			]);

			$order->user()->associate($this->auth->user);
			$order->vin()->associate($vin);
			$order->save();	

			if($this->auth->user()->telegram()->exists()) {
				$telegram = $this->auth->user()->telegram;

				try {
					$client = new Client([
					    'base_uri' => 'https://api.telegram.org',
					    'timeout'  => 10.0,
					]);

			    	$guzzle_response = $client->request('GET', '/bot' . $settings['telegram']['key'] . '/sendMessage', [
			        	'query' => [
			        		'chat_id' => $telegram->telegram_id,
			        		'text' => 'Заказ #' . $order->id . ' создан',
			        	],
					]);
				} catch (\GuzzleHttp\Exception\TransferException $exception) {
				    $this->logger->error('[Telegram]', [
				    	'request' => \GuzzleHttp\Psr7\Message::toString($exception->getRequest()),
				    	'response' => \GuzzleHttp\Psr7\Message::toString($exception->getResponse()),
				    ]);
				}
			}

			if(!$price || $quota) {
				$order->update([
					'status_id' => 2,
				]);

          		if($vin->status == 0){
					$settings = $this->settings;
					$router = $this->router;

					if(!empty($settings['vin_api'])) {
						$uri = \Slim\Http\Uri::createFromString($settings['base_path']);

				        $callback_url = $router->fullUrlFor($uri, 'order.webhook', [], [
				        	'vin' => $vin->text,
				        	'request_id' => $vin->id,
				        ]);

				        $this->logger->info('[Wait response]', [
                            'callbackUrl' => $callback_url,
                            'vin' => $vin
                        ]);

						$client = new Client([
						    'base_uri' => 'https://api.vin-check.com.ua',
						    'timeout'  => 10.0,
						]);

				        $client->request('GET', '/api.php', [
				        	'query' => [
				        		'api_key' => $settings['vin_api']['key'],
				        		'action' => 'add_to_queue',
				        		'vin' => $vin->text,
				        		'translate' => 0,
				        		'callback_url' => $callback_url,
				        	],
						]);
					}
				}

				if($vin->status == 1){
					$order->update([
						'status_id' => 3,
					]);
					
					if($this->auth->user()->telegram()->exists()) {
						$telegram = $this->auth->user()->telegram;

						$filename = $settings['vin_api']['path'] . $order->vin->filename;

						if(file_exists($filename) && is_file($filename) && $this->auth->getId() != 10) {
							$client = new Client([
							    'base_uri' => 'https://api.telegram.org',
							    'timeout'  => 10.0, 
							]);

							$filename_parts = explode('.', $order->vin->filename);

							$uri = \Slim\Http\Uri::createFromString($settings['base_path']);

        					$callback_url = $router->fullUrlFor($uri, 'telegram.download', [
        						'token' => $filename_parts[0],
        					], [
        						'filename' => $order->vin->text,
        					]);
        					try {
                                $client->request('GET', '/bot' . $settings['telegram']['key'] . '/sendDocument', [
                                    'multipart' => [
                                        [
                                            'name' => 'chat_id',
                                            'contents' => $telegram->telegram_id,
                                        ],
                                        [
                                            'name' => 'document',
                                            'contents' => $callback_url,
                                        ],
                                        [
                                            'name' => 'caption',
                                            'contents' => 'Заказ #' . $order->id . ' был обработан',
                                        ],
                                    ],
                                ]);
                            } catch (Throwable $exception) {
        					    $this->logger->info('[ERROR]', [
        					        'message' => $exception->getMessage()
                                ]);
                            }
				    	}
					}
				}

				return $response->withRedirect($this->router->pathFor('order.list'));
			}

			return $response->withRedirect($this->router->pathFor('order.payment', ['id' => $order->id]));
		}

		return $this->view->render($response, 'order/create.twig', [
	        'vin' => $request->getParsedBodyParam('vin'),
	        'price' => $price,
	        'error' => $this->error,
	    ]); 
	}

	public function list(Request $request, Response $response, array $args) {
        $price = 0;
        $quota = $this->auth->getQuota();

        if(!$quota){
            $price = $this->auth->getPrice();
        }

        $ordersQuery = $this->auth->user()->orders();

        if($this->auth->isAdmin()) {
            $ordersQuery = Order::query();
        }

        if ($request->getQueryParam('searchId')) {
            $ordersQuery->where('orders.id', $request->getQueryParam('searchId'));
        }
        if ($request->getQueryParam('searchStatus')) {
            $ordersQuery->where('orders.status_id', $request->getQueryParam('searchStatus'));
        }
        if ($request->getQueryParam('searchSum')) {
            $ordersQuery->where('orders.total', $request->getQueryParam('searchSum'));
        }
        if ($request->getQueryParam('searchName')) {
            $ordersQuery->leftJoin('users', 'users.id', '=', 'orders.user_id')
                ->where('users.name', 'like', "%" . $request->getQueryParam('searchName') . "%");
        }
        if ($request->getQueryParam('searchVin')) {
            $ordersQuery->leftJoin('vin_request', 'vin_request.id', '=', 'orders.vin_id')
                ->where('vin_request.text', 'like', "%" . $request->getQueryParam('searchVin') . "%");
        }
        if ($request->getQueryParam('searchDate')) {
            $searchDate = new DateTime($request->getQueryParam('searchDate'));
            $ordersQuery->whereBetween('orders.created_at', [
                $searchDate->format('Y-m-d ') . '00:00:00',
                $searchDate->format('Y-m-d ') . '23:59:59'
            ]);
        }

		$orders = $ordersQuery->orderBy('orders.created_at', 'DESC')->paginate(25);

		return $this->view->render($response, 'order/list.twig', [
            'vin' => '',
            'price' => $price,
            'error' => [],
	        'orders' => $orders,
            'searchId' => $request->getQueryParam('searchId'),
            'searchStatus' => $request->getQueryParam('searchStatus'),
            'searchSum' => $request->getQueryParam('searchSum'),
            'searchName' => $request->getQueryParam('searchName'),
            'searchVin' => $request->getQueryParam('searchVin'),
            'searchDate' => $request->getQueryParam('searchDate'),
            'total' => $orders->total(),
	    ]); 
	}

	public function payment(Request $request, Response $response, array $args) {
		$order = Order::findOrFail($args['id']);

		if($order->status_id == 1) {
			$user = $this->auth->user();

			if($order->user()->exists() && $order->user->id != $this->auth->getId()) {
				$user = $order->user;
			}

			$value = $order->total - $user->balance;

			if($value > 0) {
				return $this->view->render($response, 'order/payment.twig', [
			        'value' => $value,
			        'order_id' => $order->id,
			        'balance' => $user->balance,
			        'name' => $user->name,
			    ]); 
			}

			$transaction = new Transaction([
				'value' => -$order->total, 
				'status' => 1,
			]);

			$transaction->user()->associate($user);
			
			$transaction->order()->associate($order);

			$transaction->save();

			$order->update([
				'status_id' => 2,
			]);

			if($order->vin()->exists()) {
				if($order->vin->status == 1) {
					$order->update([
						'status_id' => 3,
					]);
				}

				if($order->vin->status == 2) {
					$order->update([
						'status_id' => 4,
					]);

					$transaction->update([
						'status' => 3,
					]);
				}
			}
	    }

	    return $response->withRedirect($this->router->pathFor('order.list'));
	}

	public function decline(Request $request, Response $response, array $args) {
		$order = Order::findOrFail($args['id']);

		$order->update([
			'status_id' => 4,
		]);

		if($order->transactions()->exists()){
			foreach($order->transactions as $transaction){
				if($transaction->value < 0){
					$transaction->update([
						'status' => 2,
					]);
				}
			}
		}

	    return $response->withRedirect($this->router->pathFor('order.list'));
	}

	public function confirm(Request $request, Response $response, array $args) {
		$order = Order::findOrFail($args['id']);
        $settings = $this->settings;
        $router = $this->router;

		$order->update([
			'status_id' => 2,
		]);

		$vin = $order->vin;

		if($vin->status == 1){
			$order->update([
				'status_id' => 3,
			]);
			
			if($order->user->telegram()->exists()) {
				$telegram = $order->user->telegram;

				$filename = $settings['vin_api']['path'] . $order->vin->filename;

				if(file_exists($filename) && is_file($filename)) {
					$client = new Client([
					    'base_uri' => 'https://api.telegram.org',
					    'timeout'  => 10.0, 
					]);

					$filename_parts = explode('.', $order->vin->filename);

					$uri = \Slim\Http\Uri::createFromString($settings['base_path']);

					$callback_url = $router->fullUrlFor($uri, 'telegram.download', [
						'token' => $filename_parts[0],
					], [
						'filename' => $order->vin->text,
					]);

                    try {
                        $client->request('GET', '/bot' . $settings['telegram']['key'] . '/sendDocument', [
                            'multipart' => [
                                [
                                    'name' => 'chat_id',
                                    'contents' => $telegram->telegram_id,
                                ],
                                [
                                    'name' => 'document',
                                    'contents' => $callback_url,
                                ],
                                [
                                    'name' => 'caption',
                                    'contents' => 'Заказ #' . $order->id . ' был обработан',
                                ],
                            ],
                        ]);
                    } catch (Throwable $exception) {
                        $this->logger->info('[ERROR]', [
                            'message' => $exception->getMessage()
                        ]);
                    }
		    	}
			}

			return $response->withRedirect($this->router->pathFor('order.list'));
		}

		$settings = $this->settings;
		$router = $this->router;

		if(!empty($settings['vin_api'])) {
			$uri = \Slim\Http\Uri::createFromString($settings['base_path']);

	        $callback_url = $router->fullUrlFor($uri, 'order.webhook', [], [
	        	'vin' => $vin->text,
	        	'request_id' => $vin->id,
	        ]);

            $this->logger->info('[Wait response]', [
                'callbackUrl' => $callback_url,
                'vin' => $vin
            ]);

			$client = new Client([
			    'base_uri' => 'https://api.vin-check.com.ua',
			    'timeout'  => 10.0,
			]);

	        $client->request('GET', '/api.php', [
	        	'query' => [
	        		'api_key' => $settings['vin_api']['key'],
	        		'action' => 'add_to_queue',
	        		'vin' => $vin->text,
	        		'translate' => 0,
	        		'callback_url' => $callback_url,
	        	],
			]);
		}

	    return $response->withRedirect($this->router->pathFor('order.list'));
	}

	public function info(Request $request, Response $response, array $args) {
		$order = Order::find($args['id']);
		
		return $this->view->render($response, 'order/info.twig', [
	        'order' => $order,
	    ]); 
	}

	protected function validate(Request $request){
		if (utf8_strlen(trim($request->getParsedBodyParam('vin'))) != 17) {
			$this->error['vin'] = 'VIN must 17 symbol';
		}

		return !$this->error;
	}
}
