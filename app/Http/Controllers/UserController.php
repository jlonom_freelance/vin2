<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use DateTime;
use libphonenumber\PhoneNumberUtil;
use RuntimeException;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\User;
use App\Models\Group;
use App\Models\Transaction;

class UserController extends Controller
{
	private $error = array();

	public function list(Request $request, Response $response, array $args) {
		$users = User::query()
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'DESC')
            ->paginate(15);

		return $this->view->render($response, 'users/list.twig', [
	        'users' => $users,
	    ]); 
	}

	public function confirmEmail(Request $request, Response $response, array $args)
    {
        $user = User::findByToken($args['token']);
        $user->update([
            'token' => null,
            'email_verified_at' => (new DateTime())->format('Y-m-d H:i:s')
        ]);

        $this->auth->loginByUser($user);

        return $this->view->render($response, 'auth/email_confirmed_success.twig');
    }

	public function info(Request $request, Response $response, array $args) {
		$user = User::findOrFail($args['id']);

		$telegram_name = '';

		$telegram = $user->telegram;

		if($telegram){
			$telegram_name = $telegram->name;
		}

		$transactions = $user->transactions()->orderBy('created_at', 'DESC')->paginate(10);

	    return $this->view->render($response, 'users/info.twig', [
	    	'id' => $user->id,
			'balance' => $user->balance,
			'name' => $user->name,
			'telephone' => $user->telephone,
			'email' => $user->email,
			'telegram' => $telegram_name,
			'transactions' => $transactions,
		]);
	}

	public function add(Request $request, Response $response, array $args) {
		if($request->isPost() && $this->validate($request)){
			$hash = password_hash($request->getParsedBodyParam('password'), PASSWORD_BCRYPT, [
	            'cost' => 10,
	        ]);

			$user = new User([
				'status' => 1,
				'name' => $request->getParsedBodyParam('name'), 
				'email' => utf8_strtolower($request->getParsedBodyParam('email')), 
				'password' => $hash,
			]);

			$group = Group::find($request->getParsedBodyParam('group_id'));

			$user->group()->associate($group);
			$user->save();

			if($request->getParsedBodyParam('balance')){
				$transaction = new Transaction([
					'token' => token(32),
					'status' => 1,
					'value' => $request->getParsedBodyParam('balance'), 
				]);

				$transaction->user()->associate($user);
				$transaction->creator()->associate($this->auth->user());

				$transaction->save();
			}

			return $response->withRedirect($this->router->pathFor('user.list'));
		}

		return $this->view->render($response, 'users/add.twig', [
			'groups' => Group::all(),
			'group_id' => $request->getParsedBodyParam('group_id'),
			'name' => $request->getParsedBodyParam('name'),
			'email' => $request->getParsedBodyParam('email'),
			'password' => $request->getParsedBodyParam('password'),
			'confirm' => $request->getParsedBodyParam('confirm'),
			'balance' => $request->getParsedBodyParam('balance'),
			'error' => $this->error,
		]);
	}

	public function edit(Request $request, Response $response, array $args) {
		$user = User::findOrFail($args['id']);

		if($request->isPost() && $this->validate($request, $user)){
			$user->update([
				'name' => $request->getParsedBodyParam('name'), 
				'email' => utf8_strtolower($request->getParsedBodyParam('email')),
			]);

			if($request->getParsedBodyParam('password')){
				$hash = password_hash($request->getParsedBodyParam('password'), PASSWORD_BCRYPT, [
		            'cost' => 10,
		        ]);

				$user->update([
					'password' => $hash,
				]);
			}

			if($request->getParsedBodyParam('balance')){
				$transaction = new Transaction([
					'token' => token(32),
					'status' => 1,
					'value' => $request->getParsedBodyParam('balance'), 
				]);

				$transaction->user()->associate($user);
				$transaction->creator()->associate($this->auth->user());

				$transaction->save();
			}

			$group = Group::find($request->getParsedBodyParam('group_id'));

			$user->group()->associate($group)->save();

			return $response->withRedirect($this->router->pathFor('user.list'));
		}

		return $this->view->render($response, 'users/edit.twig', [
			'id' => $user->id,
			'groups' => Group::all(),
			'group_id' => $request->getParsedBodyParam('group_id') ? $request->getParsedBodyParam('group_id') : $user->group->id,
			'name' => $request->getParsedBodyParam('name') ? $request->getParsedBodyParam('name') : $user->name,
			'email' => $request->getParsedBodyParam('email') ? $request->getParsedBodyParam('email') : $user->email,
			'password' => $request->getParsedBodyParam('password') ? $request->getParsedBodyParam('password') : '',
			'confirm' => $request->getParsedBodyParam('confirm') ? $request->getParsedBodyParam('confirm') : '',
			'balance' => $request->getParsedBodyParam('balance') ? $request->getParsedBodyParam('balance') : $user->getBalanceAttribute(),
			'telephone' => $request->getParsedBodyParam('telephone') ? $request->getParsedBodyParam('telephone') : $user->telephone,
			'error' => $this->error,
		]);
	}

	public function delete(Request $request, Response $response, array $args) {
		$user = User::findOrFail($args['id']);
		if (!$user->delete()) {
		    throw new RuntimeException('Невозможно удалить пользователя');
        }
        return $response->withRedirect($this->router->pathFor('user.list'));
	}

	protected function validate(Request $request, $current = null){
		if ((utf8_strlen(trim($request->getParsedBodyParam('name'))) < 1) || (utf8_strlen(trim($request->getParsedBodyParam('name'))) > 32)) {
			$this->error['name'] = 'Name must be from 4 to 32 symbol';
		}

		if ((utf8_strlen($request->getParsedBodyParam('email')) > 96) || !filter_var($request->getParsedBodyParam('email'), FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = 'Email must be valid';
		}

		$user = User::where('email', '=', utf8_strtolower($request->getParsedBodyParam('email')))->first();

		if(!$current){
			if($user){
				$this->error['email'] = 'Email exists';
			}
		}else{
			if($user && $current->id != $user->id){
				$this->error['email'] = 'Email exists';
			}
		}

		if($request->getParsedBodyParam('balance')){
			if($current && $user && $current->id == $user->id){
				if(($user->balance + $request->getParsedBodyParam('balance')) < 0){
					$this->error['balance'] = 'Нельзя делать отрицательный баланс';
				}
			}
		}

		if($request->getParsedBodyParam('group_id')) {
			$group = Group::find($request->getParsedBodyParam('group_id'));

			if(!$group) {
				$this->error['group'] = 'Группа должна существовать';
			}

			if($current && $user && $group && $current->id == $user->id){
				if($this->auth->getId() == $current->id && $current->group->id != $group->id) {
					$this->error['group'] = 'Нельзя менять свою группу, будет потеря доступа';
				}
			}
		}

		if ($request->getParsedBodyParam('password') || !$current) {
			if ((utf8_strlen(html_entity_decode($request->getParsedBodyParam('password'), ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($request->getParsedBodyParam('password'), ENT_QUOTES, 'UTF-8')) > 40)) {
				$this->error['password'] = 'Password must be from 4 to 40 symbol';
			}

			if ($request->getParsedBodyParam('password') != $request->getParsedBodyParam('confirm')) {
				$this->error['confirm'] = 'Confirm must be valid';
			}
		}

        if (($phone = $request->getParsedBodyParam('telephone')) !== null) {
            $phoneNumberUtil = PhoneNumberUtil::getInstance();
            $phoneNumber = $phoneNumberUtil->parse($phone, 'UA');
            if (!$phoneNumberUtil->isValidNumber($phoneNumber)) {
                $this->error['telephone'] = 'Invalid phone number';
            }
        }

		return !$this->error;
	}
}