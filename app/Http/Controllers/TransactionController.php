<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Order;
use App\Models\Transaction;

class TransactionController extends Controller
{
	private $error = array();

	public function info(Request $request, Response $response, array $args) {
		$transaction = Transaction::findOrFail($args['id']);

		$order_id = 0;
		$order_vin = '';
		$order_status = '';

		if($transaction->order()->exists()){
			$order = $transaction->order;

			$order_id = $order->id;

			if($order->vin()->exists()){
				$order_vin = $order->vin->text;
			}

			$order_status = $order->status;
		}

		return $this->view->render($response, 'auth/transaction_info.twig', [
			'id' => $transaction->id,
	        'value' => $transaction->value,
	        'status' => $transaction->status,
	        'created_at' => $transaction->created_at,
	        'updated_at' => $transaction->updated_at,
	        'order_id' => $order_id,
	        'order_vin' => $order_vin,
	        'order_status' => $order_status,
	    ]);
	}

	public function create(Request $request, Response $response, array $args) {
		if($this->validate($request)){
			$transaction = new Transaction([
				'token' => token(32),
				'value' => $request->getParsedBodyParam('value'), 
			]);

			$transaction->user()->associate($this->auth->user());
			$transaction->creator()->associate($this->auth->user());

			if($request->getParsedBodyParam('order_id')){
				$order = Order::find($request->getParsedBodyParam('order_id'));

				if($order->user()->exists() && $order->user->id != $this->auth->getId()) {
					$transaction->user()->associate($order->user);
				}

				$transaction->order()->associate($order);
			}

			$transaction->save();

			return $response->withRedirect($this->router->pathFor('transaction.confirm', ['id' => $transaction->id]));
		}

		return $response->withRedirect($this->router->pathFor('auth.info'));
	}

	public function confirm(Request $request, Response $response, array $args) {
		$transaction = Transaction::findOrFail($args['id']);

		$settings = $this->settings;
		$private_key = $settings['payment']['private'];
        $public_key = $settings['payment']['public'];
        $currency = $settings['payment']['currency'];
        $language = $settings['payment']['language'];

		$uri = \Slim\Http\Uri::createFromString($settings['base_path']);

        $order_id = $transaction->token;

        if($transaction->order()->exists()){
        	$description = 'Оплата заказа #' . $transaction->order->id;
        } else {
        	$description = 'Пополнение баланса пользователя ' . implode('#', [$transaction->user->name, $transaction->user->id]);
        }

        $result_url = $this->router->fullUrlFor($uri, 'transaction.return', ['id' => $transaction->id]);
    	$server_url = $this->router->fullUrlFor($uri, 'transaction.callback', ['id' => $transaction->id]);

        $amount = number_format($transaction->value, 2, '.', '');

        $send_data = [
        	'version'=> 3,
            'public_key' => $public_key,
            'amount' => $amount,
            'currency' => $currency,
            'description' => $description,
            'order_id' => $order_id,
            'action' => 'pay',
            'language' => $language,
            'server_url' => $server_url,
            'result_url' => $result_url
        ];

        $liqpay_data = base64_encode(json_encode($send_data));
        $liqpay_signature = base64_encode(sha1($private_key . $liqpay_data . $private_key, true));
	
		return $this->view->render($response, 'auth/transaction.twig', [
	        'total' => $amount,
	        'user' => $transaction->user->name,
	        'send_data' => $send_data,
	        'liqpay_data' => $liqpay_data,
	        'liqpay_signature' => $liqpay_signature,
	        'action' => 'https://www.liqpay.ua/api/3/checkout',
	    ]); 
		
	}

	public function callback(Request $request, Response $response, array $args) {
		$transaction = Transaction::find($args['id']);

		$this->logger->info('[Payment]', [
			'body' => $request->getParsedBody(),
			'query' => $request->getQueryParams(),
		]);

		$settings = $this->settings;
		$private_key = $settings['payment']['private'];

		if($data = $request->getParsedBodyParam('data')) {
        	$signature = base64_encode(sha1($private_key . $data . $private_key, true));

	        $parsed_data = json_decode(base64_decode($data), true);

	        $this->logger->info('[Payment]', [
				'data' => $parsed_data,
			]);

	        $order_id = 0;
	        $status = '';

	        if($parsed_data) {
	        	if(!empty($parsed_data['order_id'])) {
	        		$order_id = $parsed_data['order_id'];
	        	}

	        	if(!empty($parsed_data['order_id'])) {
	        		$status = $parsed_data['status'];
	        	}
	        }

	        if ($order_id == $transaction->token && $signature == $request->getParsedBodyParam('signature')) {
	        	if($status == 'success'){
	        		$transaction->update([
		        		'status' => 1,
		        	]);

		        	if($transaction->order()->exists()) {
		        		$order = $transaction->order;

		        		if($order->status_id == 1){
		        			$transaction = new Transaction([
								'value' => -$order->total, 
								'status' => 1,
							]);

							$transaction->user()->associate($order->user);
							$transaction->order()->associate($order);

							$transaction->save();
		        		}

		        		$order->update([
		        			'status_id' => 2,
		        		]);

		        		if($order->vin()->exists() && $order->vin->status == 1){
			        		$order->update([
			        			'status_id' => 3,
			        		]);
		        		}
		        	}
	        	}

	        	if($status == 'error' || $status == 'failure'){
	        		$transaction->update([
		        		'status' => 2,
		        	]);
	        	}
	        }
		}

		return $response;
	}

	public function return(Request $request, Response $response, array $args) {
		$transaction = Transaction::findOrFail($args['id']);
		
        $this->logger->info('[Payment return]', [
			'body' => $request->getParsedBody(),
			'query' => $request->getQueryParams(),
		]);

		$settings = $this->settings;
		$private_key = $settings['payment']['private'];

		if($data = $request->getParsedBodyParam('data')) {
        	$signature = base64_encode(sha1($private_key . $data . $private_key, true));

	        $parsed_data = json_decode(base64_decode($data), true);

	        $this->logger->info('[Payment return]', [
				'data' => $parsed_data,
			]);

			$order_id = 0;
	        $status = '';

			if($parsed_data) {
	        	if(!empty($parsed_data['order_id'])) {
	        		$order_id = $parsed_data['order_id'];
	        	}

	        	if(!empty($parsed_data['order_id'])) {
	        		$status = $parsed_data['status'];
	        	}
	        }

	        if ($order_id == $transaction->token && $signature == $request->getParsedBodyParam('signature')) {
	        	if($status == 'success'){
	        		$transaction->update([
		        		'status' => 1,
		        	]);

		        	if($transaction->order()->exists()) {
		        		$order = $transaction->order;

		        		if($order->status_id == 1){
		        			$minus_transaction = new Transaction([
								'value' => -$order->total, 
								'status' => 1,
							]);

							$minus_transaction->user()->associate($order->user);
							$minus_transaction->order()->associate($order);

							$minus_transaction->save();
		        		}

		        		$order->update([
		        			'status_id' => 2,
		        		]);

		        		if($order->vin()->exists()) {
		        			if($order->vin->status == 1){
				        		$order->update([
				        			'status_id' => 3,
				        		]);
			        		} else {
								$this->vin_api->add($order->vin);
							}
		        		}
		        	}
	        	}

	        	if($status == 'error' || $status == 'failure') {
	        		$transaction->update([
		        		'status' => 2,
		        	]);
	        	}
	        }
	    }

	    return $response->withRedirect($this->router->pathFor('transaction.info', ['id' => $transaction->id]));
	}

	public function check(Request $request, Response $response, array $args) {
		$transaction = Transaction::findOrFail($args['id']);

		if($transaction->status == 1){
			return $response->withRedirect($this->router->pathFor('transaction.info', ['id' => $transaction->id]));
		}
		
		$settings = $this->settings;
		$private_key = $settings['payment']['private'];
		$public_key = $settings['payment']['public'];

		$send_data = [
        	'version'=> 3,
            'public_key' => $public_key,
            'order_id' => $transaction->token,
            'action' => 'status',
        ];

        $liqpay_data = base64_encode(json_encode($send_data));
    	$liqpay_signature = base64_encode(sha1($private_key . $liqpay_data . $private_key, true));

    	$guzzle = new \GuzzleHttp\Client([
		    'base_uri' => 'https://www.liqpay.ua',
		    'timeout'  => 10.0,
		]);

        $guzzle_response = $guzzle->request('POST', '/api/request', [
		    'form_params' => [
		        'data' => $liqpay_data,
		        'signature' => $liqpay_signature
		    ]
		]);

        $body = $guzzle_response->getBody();

        $return = json_decode($body, true);

        $this->logger->info('[Payment Check]', [
			'response' => $return,
		]);

		$order_id = 0;
        $status = '';

        if($return){
        	if(!empty($return['status'])){
        		$status = $return['status'];
        	}

        	if(!empty($parsed_data['order_id'])) {
        		$order_id = $parsed_data['order_id'];
        	}
        }

        if($status == 'success') {
        	$transaction->update([
        		'status' => 1,
        	]);

        	if($transaction->order()->exists()) {
        		$order = $transaction->order;

        		if($order->status_id == 1){
        			$minus_transaction = new Transaction([
						'value' => -$order->total, 
						'status' => 1,
					]);

					$minus_transaction->user()->associate($order->user);
					$minus_transaction->order()->associate($order);

					$minus_transaction->save();
        		}

        		$order->update([
        			'status_id' => 2,
        		]);

        		if($order->vin()->exists()) {
        			if($order->vin->status == 1){
		        		$order->update([
		        			'status_id' => 3,
		        		]);
	        		} else {
		        		$this->vin_api->add($order->vin);
					}
        		}
        	}
        }

        if($status == 'error' || $status == 'failure') {
        	$transaction->update([
        		'status' => 2,
        	]);
        }

        return $response->withRedirect($this->router->pathFor('transaction.info', ['id' => $transaction->id]));
	}

	protected function validate($request){
		if($request->getParsedBodyParam('value') <= 0 || $request->getParsedBodyParam('value') > 9999) {
			$this->error['value'] = 'Invalid value';
		}

		if($request->getParsedBodyParam('order_id')){
			$order = Order::find($request->getParsedBodyParam('order_id'));

			if(!$order){
				$this->error['order'] = 'Invalid Order ID';
			}
		}

		return !$this->error;
	}
}