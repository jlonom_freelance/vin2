<?php

namespace App\Http\Controllers;

use App\Http\Controller;

class FilesController extends Controller
{
    public function removeOldFiles()
    {
        $settings = $this->settings;
        $filesList = [];
        $count = 0;
        $this->recursiveScan($settings['vin_api']['path'], $filesList);
        foreach ($filesList as $item) {
            if (filectime($item) + env('CACHE_LIFETIME') < time()) {
                unlink($item);
                $count++;
            }
        }
        return $count;
    }

    private function recursiveScan($path, &$filesList)
    {
        $path = rtrim($path, '/');
        if(is_dir($path)) {
            $files = scandir($path);
            foreach($files as $file) {
                if($file != '.' && $file != '..') {
                    $this->recursiveScan($path . '/' . $file, $filesList);
                }
            }
        } else {
            $filesList[] = $path;
        }
    }
}