<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use DateTime;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\User;
use App\Models\Group;
use App\Models\Order;
use App\Models\Transaction;

class StatisticsController extends Controller
{
	public function index(Request $request, Response $response, array $args) {
		$user_id = $request->getQueryParam('user_id', 0);
		$group_id = $request->getQueryParam('group_id', 0);
        $dateFrom = $request->getQueryParam('dateFrom', '1970-01-01') . ' 00:00:00';
        $dateTo = $request->getQueryParam('dateTo', (new DateTime())->format('Y-m-d')) . ' 23:59:59';

		$user = User::find($user_id);
		$group = Group::find($group_id);

		$count = 0;
		$count_free = 0;
		$count_paid = 0;
		$total = 0;
		$unused = 0;

		if($user){
			$count = Order::where('status_id', '=', 3)
                ->where('user_id', '=', $user->id)
                ->where('created_at', '>', $dateFrom )
                ->where('created_at', '<', $dateTo )
                ->count();
			$count_free = Order::where('total', '=', 0)
                ->where('status_id', '=', 3)
                ->where('user_id', '=', $user->id)
                ->where('created_at', '>', $dateFrom )
                ->where('created_at', '<', $dateTo )
                ->count();
			$count_paid = Order::where('total', '>', 0)
                ->where('status_id', '=', 3)
                ->where('user_id', '=', $user->id)
                ->where('created_at', '>', $dateFrom )
                ->where('created_at', '<', $dateTo )
                ->count();

			$total = $user->transactions()->where('status', '=', '1')->where('value', '>', 0)->sum('value');
			$unused = $user->balance;
		}elseif($group ){
			$users = $group->users()->withCount([
				'orders' =>  function ($query) use ($dateFrom, $dateTo) {
				    return $query->where('status_id', '=', 3)
                        ->where('created_at', '>', $dateFrom )
                        ->where('created_at', '<', $dateTo );
				},
				'orders as orders_count_free' =>  function ($query) use ($dateFrom, $dateTo) {
				    return $query->where('total', '=', 0)
                        ->where('created_at', '>', $dateFrom )
                        ->where('created_at', '<', $dateTo )
                        ->where('status_id', '=', 3);
				},
				'orders as orders_count_paid' =>  function ($query) use ($dateFrom, $dateTo) {
				    return $query->where('total', '>', 0)
                        ->where('created_at', '>', $dateFrom )
                        ->where('created_at', '<', $dateTo )
                        ->where('status_id', '=', 3);
				},
			])->with([
				'transactions' =>  function ($query) use ($dateFrom, $dateTo)  {
				    return $query->where('status', '=', '1')
                        ->where('created_at', '>', $dateFrom )
                        ->where('created_at', '<', $dateTo )
                        ->where('value', '>', 0);
				}
			])->get();

			foreach($users as $user){
				$count += $user->orders_count;
				$count_free += $user->orders_count_free;
				$count_paid += $user->orders_count_paid;
				$total += $user->transactions->sum('value');
				$unused += $user->balance;
			}
		}else{
			$count = Order::where('status_id', '=', 3)
                ->where('created_at', '>', $dateFrom )
                ->where('created_at', '<', $dateTo )
                ->count();
			$count_free = Order::where('total', '=', 0)
                ->where('created_at', '>', $dateFrom )
                ->where('created_at', '<', $dateTo )
                ->where('status_id', '=', 3)->count();
			$count_paid = Order::where('total', '>', 0)
                ->where('created_at', '>', $dateFrom )
                ->where('created_at', '<', $dateTo )
                ->where('status_id', '=', 3)->count();

			$users = User::with(['transactions' =>  function ($query) use ($dateFrom, $dateTo) {
			    return $query->where('status', '=', '1')->where('value', '>', 0)
                    ->where('created_at', '>', $dateFrom )
                    ->where('created_at', '<', $dateTo );
			}])->get();

			foreach($users as $user){
				$total += $user->transactions->sum('value');
				$unused += $user->balance;
			}
		}

		return $this->view->render($response, 'statistics/index.twig', [
			'count' => $count,
			'count_free' => $count_free,
			'count_paid' => $count_paid,
			'total' => $total,
			'unused' => $unused,
			'user_id' => $request->getQueryParam('user_id', 0),
			'group_id' => $request->getQueryParam('group_id', 0),
            'dateFrom' => $request->getQueryParam('dateFrom'),
            'dateTo' => $request->getQueryParam('dateTo'),
			'users' => User::all(),
			'groups' => Group::all(),
		]); 
	}
}