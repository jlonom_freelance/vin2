<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Group;

/**
 * Class GroupController
 *
 * @package App\Http\Controllers
 * @property \Slim\Flash\Messages $flash
 */
class GroupController extends Controller
{
	private $error = array();

	public function list(Request $request, Response $response, array $args) {
		$groups = Group::orderBy('created_at', 'DESC')->paginate(15);

		return $this->view->render($response, 'group/list.twig', [
	        'groups' => $groups,
            'flashMessages' => $this->flash->getMessages()
	    ]); 
	}

	public function add(Request $request, Response $response, array $args) {
		if($request->isPost() && $this->validate($request)){
			$group = new Group([
				'name' => $request->getParsedBodyParam('name'), 
				'price' => $request->getParsedBodyParam('price'), 
				'month' => $request->getParsedBodyParam('month') ? $request->getParsedBodyParam('month') : 0,
				'day' => $request->getParsedBodyParam('day') ? $request->getParsedBodyParam('day') : 0,
			]);

			$group->save();

			return $response->withRedirect($this->router->pathFor('group.list'));
		}

		return $this->view->render($response, 'group/add.twig', [
			'name' => $request->getParsedBodyParam('name', ''),
			'price' => $request->getParsedBodyParam('price', ''),
			'day' => $request->getParsedBodyParam('day', ''),
			'month' => $request->getParsedBodyParam('month', ''),
			'error' => $this->error,
		]);
	}

	public function edit(Request $request, Response $response, array $args) {
		$group = Group::findOrFail($args['id']);

		if($request->isPost() && $this->validate($request, $group->id)){
			$group->update([
				'name' => $request->getParsedBodyParam('name'), 
				'price' => $request->getParsedBodyParam('price'), 
				'month' => $request->getParsedBodyParam('month'),
				'day' => $request->getParsedBodyParam('day'),
			]);

			return $response->withRedirect($this->router->pathFor('group.list'));
		}

		return $this->view->render($response, 'group/edit.twig', [
			'id' => $group->id,
			'name' => $request->getParsedBodyParam('name') ? $request->getParsedBodyParam('name') : $group->name,
			'price' => $request->getParsedBodyParam('price') ? $request->getParsedBodyParam('price') : $group->price,
			'day' => $request->getParsedBodyParam('day') ? $request->getParsedBodyParam('day') : $group->day,
			'month' => $request->getParsedBodyParam('month') ? $request->getParsedBodyParam('month') : $group->month,
			'error' => $this->error,
		]);
	}

	public function delete(Request $request, Response $response, array $args)
    {
        $group = Group::findOrFail($args['id']);
        if (count($group->users) > 0) {
            $this->flash->addMessage('danger', 'Сначала надо удалить пользователей из группы');
        } else {
            $group->delete();
            $this->flash->addMessage('success', 'Группа удалена');
        }

        return $response->withRedirect($this->router->pathFor('group.list'));
    }

	protected function validate(Request $request, $user_id = null){
		if ((utf8_strlen(trim($request->getParsedBodyParam('name'))) < 1) || (utf8_strlen(trim($request->getParsedBodyParam('name'))) > 32)) {
			$this->error['name'] = 'Name must be from 4 to 32 symbol';
		}

		if ($request->getParsedBodyParam('price') && $request->getParsedBodyParam('price') < 0) {
			$this->error['price'] = 'Price must be more than 0';
		}

		if($request->getParsedBodyParam('month') != 0 && $request->getParsedBodyParam('day') != 0 && $request->getParsedBodyParam('day') > $request->getParsedBodyParam('month')){
			$this->error['day'] = 'Дневная квота должна быть меньше чем месячная';
		}

		return !$this->error;
	}
}