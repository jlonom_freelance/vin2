<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use App\Models\Page;
use Slim\Http\Request;
use Slim\Http\Response;

class PageController extends Controller
{
    private $error = array();

    public function list(Request $request, Response $response, array $args): Response
    {
        $pages = Page::orderBy('created_at', 'DESC')->paginate(25);

        return $this->view->render($response, 'pages/list.twig', [
            'pages' => $pages,
        ]);
    }

    public function view(Request $request, Response $response, array $args): Response
    {
        $slug = $args['slug'];

        $page = Page::findBySlug($slug);

        return $this->view->render($response, 'pages/view.twig', [
            'page' => $page,
        ]);
    }

    public function update(Request $request, Response $response, array $args): Response
    {
        $page = Page::findOrFail($args['id']);

        if($request->isPost() && $this->validate($request, $page)){
            $page->update([
                'title' => $request->getParsedBodyParam('title'),
                'content' => $request->getParsedBodyParam('content'),
                'seo_title' => $request->getParsedBodyParam('seo_title'),
                'seo_description' => $request->getParsedBodyParam('seo_description')
            ]);

            return $response->withRedirect($this->router->pathFor('page.list'));
        }

        return $this->view->render($response, 'pages/edit.twig', [
            'title' => $request->getParsedBodyParam('title') ? $request->getParsedBodyParam('title') : $page->title,
            'content' => $request->getParsedBodyParam('content') ? $request->getParsedBodyParam('content') : $page->content,
            'seo_title' => $request->getParsedBodyParam('seo_title') ? $request->getParsedBodyParam('seo_title') : $page->seo_title,
            'seo_description' => $request->getParsedBodyParam('seo_description') ? $request->getParsedBodyParam('seo_description') : $page->seo_description,
            'error' => $this->error,
        ]);
    }

    public function store(Request $request, Response $response, array $args): Response
    {
        $page = new Page;
        if($request->isPost() && $this->validate($request, $page)){
            $page->create([
                'title' => $request->getParsedBodyParam('title'),
                'slug' => $page->generateSlugByTitle($request->getParsedBodyParam('title')),
                'content' => $request->getParsedBodyParam('content'),
                'seo_title' => $request->getParsedBodyParam('seo_title'),
                'seo_description' => $request->getParsedBodyParam('seo_description')
            ]);

            return $response->withRedirect($this->router->pathFor('page.list'));
        }

        return $this->view->render($response, 'pages/create.twig', [
            'title' => $request->getParsedBodyParam('title') ? $request->getParsedBodyParam('title') : $page->title,
            'content' => $request->getParsedBodyParam('content') ? $request->getParsedBodyParam('content') : $page->content,
            'seo_title' => $request->getParsedBodyParam('seo_title') ? $request->getParsedBodyParam('seo_title') : $page->seo_title,
            'seo_description' => $request->getParsedBodyParam('seo_description') ? $request->getParsedBodyParam('seo_description') : $page->seo_description,
            'error' => $this->error,
        ]);
    }

    protected function validate(Request $request, Page $page)
    {
        if ((utf8_strlen(trim($request->getParsedBodyParam('title'))) < 1) || (utf8_strlen(trim($request->getParsedBodyParam('title'))) > 255)) {
            $this->error['title'] = 'Название должно содержать от 4 до 255 символов';
        }

        if (utf8_strlen(trim($request->getParsedBodyParam('content'))) < 1) {
            $this->error['content'] = 'Содержание страницы не может быть пустым';
        }

        if ((utf8_strlen(trim($request->getParsedBodyParam('seo_title'))) < 1)
            || (utf8_strlen(trim($request->getParsedBodyParam('seo_title'))) > 255)
        ) {
            $this->error['seo_title'] = 'SEO-Название должно содержать от 1 до 255 символов';
        }

        if ((utf8_strlen(trim($request->getParsedBodyParam('seo_description'))) < 1)
            || (utf8_strlen(trim($request->getParsedBodyParam('seo_description'))) > 255)
        ) {
            $this->error['seo_description'] = 'SEO-Описание должно содержать от 1 до 255 символов';
        }

        return !$this->error;
    }
}