<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\User;

class HomeController extends Controller
{
	public function index(Request $request, Response $response, array $args): Response
    {
	    return $response->withRedirect('/order');
	}
}