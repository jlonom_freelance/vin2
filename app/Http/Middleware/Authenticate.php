<?php

namespace App\Http\Middleware;

use App\Http\Middleware;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Container;

class Authenticate extends Middleware
{
	/**
     * Except on routes
     *
     * @var array
     */
    protected $except = []; 

	public function handle(Request $request, Response $response, callable $next) {
		if($this->container instanceof Container){
			if(!$this->auth->isLogged()){
				return $response->withRedirect($this->router->pathFor('auth.login'), 302);
			}

			return $next($request, $response);
		}

	    return $response->withStatus(500);
	}
}