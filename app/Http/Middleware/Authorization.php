<?php

namespace App\Http\Middleware;

use App\Http\Middleware;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Container;

class Authorization extends Middleware
{
	/**
     * Except on routes
     *
     * @var array
     */
    protected $except = []; 

	public function handle(Request $request, Response $response, callable $next) {
		if($this->container instanceof Container){
			if(!$this->auth->isAdmin()){
				return $this->view->render($response, 'forbidden.twig', [], 403);
			}

			return $next($request, $response);
		}

	    return $response->withStatus(500);
	}
}