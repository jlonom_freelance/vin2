<?php

namespace App\Services;

use App\Models\Order;
use App\Models\VinRequest;
use GuzzleHttp\Client;
use Slim\Http\Uri;
use Slim\Interfaces\RouterInterface;
use Throwable;

class VinCheckApiService
{
    protected Client $client;
    protected string $apiKey;
    protected string $telegramBotApiKey;
    protected array $canceledStatuses = [4, 5, 7, 8];
    protected string $filepath;
    protected string $basePath;
    protected RouterInterface $router;
    protected bool $sendTelegramNotify;

    public function __construct(array $settings, RouterInterface $router, $sendTelegramNotify = false)
    {
        $this->client = new Client([
            'base_uri' => 'https://api.vin-check.com.ua',
            'timeout'  => 10.0,
        ]);

        $this->apiKey = env('VIN_API_KEY');
        $this->telegramBotApiKey = env('TELEGRAM_BOT_KEY');
        $this->filepath = $settings['vin_api']['path'];
        $this->basePath = $settings['base_path'];
        $this->router = $router;
        $this->sendTelegramNotify = $sendTelegramNotify;
    }

    public function getData(VinRequest $vin): bool|string
    {
        $checkReport = $this->checkReport($vin->text);

        if ($checkReport['success'] === true) {
            $file = $this->makeFile($vin);
            $this->updateOrders($vin);
            return $file;
        } else {
            $this->processErrors($checkReport, $vin);
            return false;
        }
    }

    protected function checkReport(string $vin)
    {
        $checkReportResponse = $this->client->request('GET', '/api.php', [
            'query' => [
                'api_key' => $this->apiKey,
                'action' => 'check_report',
                'vin' => $vin,
                'translate' => 0,
            ],
        ]);

        $responseContent = $checkReportResponse->getBody()->getContents();

        return json_decode($responseContent, true);
    }

    protected function makeFile(VinRequest $vin)
    {
        $filename = !empty($vin->filename) ? $vin->filename : token(32) . '.blob';
        $resource = fopen($this->filepath . $filename, 'w');
        $this->client->get('/api.php', [
            'query' => [
                'api_key' => $this->apiKey,
                'action' => 'get_report',
                'vin' => $vin->text,
                'translate' => 0,
            ],
            'sink' => $resource,
        ]);
        $vin->update([
            'filename' => $filename,
            'status' => 1,
        ]);

        return $this->filepath . $filename;
    }

    protected function updateOrders(VinRequest $vin)
    {
        $orders = Order::where('vin_id', '=', $vin->id)->where('status_id', '=', 2)->get();

        foreach($orders as $order){
            $order->update(['status_id' => 3]);
            if($order->user->telegram()->exists()) {
                $telegram = $order->user->telegram;

                $client = new Client([
                    'base_uri' => 'https://api.telegram.org',
                    'timeout'  => 10.0,
                ]);

                $client->request('GET', '/bot' . $this->telegramBotApiKey . '/sendMessage', [
                    'query' => [
                        'chat_id' => $telegram->telegram_id,
                        'text' => 'Заказ #' . $order->id . ' обработан',
                    ],
                ]);

                $filename = $this->filepath . $order->vin->filename;

                if(file_exists($filename) && is_file($filename)) {
                    if ($this->sendTelegramNotify) {

                        $client = new Client([
                            'base_uri' => 'https://api.telegram.org',
                            'timeout'  => 10.0,
                        ]);

                        $filename_parts = explode('.', $order->vin->filename);

                        $uri = Uri::createFromString($this->basePath);

                        $callback_url = $this->router->fullUrlFor($uri, 'telegram.download', [
                            'token' => $filename_parts[0],
                        ], [
                            'filename' => $order->vin->text,
                        ]);
                        try {
                            $client->post('/bot' . $this->telegramBotApiKey . '/sendDocument', [
                                'multipart' => [
                                    [
                                        'name' => 'chat_id',
                                        'contents' => $telegram->telegram_id,
                                    ],
                                    [
                                        'name' => 'document',
                                        'contents' => $callback_url,
                                    ],
                                    [
                                        'name' => 'caption',
                                        'contents' => 'Заказ #' . $order->id . ' был обработан',
                                    ],
                                ],
                            ]);
                        } catch (Throwable $exception) {
                            echo $callback_url . "\n";
                            echo $exception->getMessage();
                        }
                    }
                }
            }
        }
    }

    protected function processErrors(array $reportData, VinRequest $vin)
    {
        if (!empty($reportData['errors']) && is_array($reportData['errors'])) {
            foreach($reportData['errors'] as $code => $error){
                if(in_array($code, $this->canceledStatuses)){
                    $vin->update([
                        'status' => 2,
                    ]);
                    $orders = Order::where('vin_id', '=', $vin->id)->get();

                    foreach($orders as $order){
                        $order->update(['status_id' => 4]);
                        $order->transactions()->where('value', '<', 0)->update(['status' => 2]);
                        if($order->user->telegram()->exists()) {
                            $telegram = $order->user->telegram;
                            $client = new Client([
                                'base_uri' => 'https://api.telegram.org',
                                'timeout'  => 10.0,
                            ]);
                            $client->request('GET', '/bot' . $this->telegramBotApiKey . '/sendMessage', [
                                'query' => [
                                    'chat_id' => $telegram->telegram_id,
                                    'text' => 'Заказ #' . $order->id . ' был отменен',
                                ],
                            ]);
                        }
                    }
                }
            }
        }
    }
}