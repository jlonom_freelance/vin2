<?php

namespace App\Services;

use PHPMailer\PHPMailer\PHPMailer;

class EmailService
{
    protected PHPMailer $mailer;

    public function __construct()
    {
        $this->mailer = new PHPMailer();
        $this->mailer->isSMTP();
        $this->mailer->Host = env('SMTP_HOST');
        $this->mailer->Port = env('SMTP_PORT');
        $this->mailer->SMTPAuth = true;
        $this->mailer->Username = env('SMTP_USER');
        $this->mailer->Password = env('SMTP_PASS');
        $this->mailer->SMTPSecure = env('SMTP_SECURE');
        $this->mailer->setFrom(env('SMTP_USER'));
        $this->mailer->CharSet = "UTF-8";
    }

    public function sendMail(string $to, string $subject,  string $htmlText): bool
    {
        $this->mailer->addAddress($to);
        $this->mailer->Subject = $subject;
        $this->mailer->msgHTML($htmlText);
        return $this->mailer->send();
    }

    public function getError()
    {
        if ($this->mailer->isError()) {
            return $this->mailer->ErrorInfo;
        }
        return false;
    }
}