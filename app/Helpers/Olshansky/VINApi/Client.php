<?php 
namespace App\Helpers\Olshansky\VINApi;

use App\Models\VinRequest;

class Client {
    const API_HOSTNAME = 'https://api.vin-check.com.ua';

    const API_PATH = '/api.php';

    private $router;
    private $client;
    private $private;
    private $uri;

    public function __construct($container)
    {
        $this->router = $container->get('router');

        $this->client = new \GuzzleHttp\Client([
            'base_uri' => self::API_HOSTNAME,
            'timeout'  => 10.0,
        ]);

        $settings = $container->get('settings');

        if(!empty($settings['vin_api']['key'])){
            $this->private = $settings['vin_api']['key'];

            $this->uri = \Slim\Http\Uri::createFromString($settings['base_path']);
        }
    }

    public function add(VinRequest $vin, $translate = 0)
    {
        $callback_url = $this->router->fullUrlFor($this->uri, 'order.webhook', [], [
            'vin' => $vin->text,
            'request_id' => $vin->id,
        ]);

        $this->client->request('GET', self::API_PATH, [
            'query' => [
                'api_key' => $this->private,
                'action' => 'add_to_queue',
                'vin' => $vin->text,
                'translate' => $translate,
                'callback_url' => $callback_url,
            ],
        ]);
    }
}