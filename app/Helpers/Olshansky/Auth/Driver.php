<?php 
namespace App\Helpers\Olshansky\Auth;

use App\Models\User;

class Driver {
    public $user;

    public function __construct($container) {
        $this->request = $container->get('request');
        $this->session = $container->get('session');

        if (isset($this->session->data['user_id'])) {
            $user = User::find($this->session->data['user_id']);

            if ($user) {
                $this->user = $user;
            } else {
                $this->logout();
            }
        }
    }

    public function login($request) {
        $user = User::query()
            ->where('email', '=', utf8_strtolower($request->getParsedBodyParam('email')))
            ->whereNotNull('email_verified_at')
            ->whereNull('token')
            ->whereNull('deleted_at')
            ->first();

        if ($user) {
            if(password_verify($request->getParsedBodyParam('password'), $user->password)){
                $this->session->data['user_id'] = $user->id;

                return true;
            }
        }

        return false;
    }
    public function loginByUser($user) {
        if ($user) {
            $this->session->data['user_id'] = $user->id;

            return true;
        }
    }

    public function logout() {
        unset($this->session->data['user_id']);
    }

    public function isLogged() {
        return $this->user() ? true : false;
    }

    public function getId() {
        return $this->isLogged() ? $this->user()->id : 0;
    }

    public function getGroupId() {
        return $this->group() ? $this->group()->id : 0;
    }

    public function getName() {
        return $this->isLogged() ? $this->user()->name : null;
    }

    public function getPrice() {
        return $this->group() ? $this->group()->price : null;
    }

    public function getEmail() {
        return $this->isLogged() ? $this->user()->email : null;
    }

    public function getBalance() {
        return $this->isLogged() ? $this->user()->balance : null;
    }

    public function getQuota() {
        $month_remains = 0;
        $day_remains = 0;

        if($this->group()) {
            if($this->group()->month) {
                $month_quota = $this->group()->month;

                $month_count = $this->user()
                    ->orders()
                    ->where('total', '=', 0)
                    ->whereYear('created_at', \Carbon\Carbon::now()->year)
                    ->whereMonth('created_at', \Carbon\Carbon::now()->month)
                    ->count();

                $month_remains = $month_quota - $month_count;

                $month_remains = ($month_remains >= 0) ? $month_remains : 0;

                if(!$this->group()->day){
                    return $month_remains;
                }
            }

            if($this->group()->day) {
                $day_quota = $this->group()->day;

                $day_count = $this->user()
                    ->orders()
                    ->where('total', '=', 0)
                    ->whereYear('created_at', \Carbon\Carbon::now()->year)
                    ->whereMonth('created_at', \Carbon\Carbon::now()->month)
                    ->whereDay('created_at', \Carbon\Carbon::now()->day)
                    ->count();

                $day_remains = $day_quota - $day_count;

                $day_remains = ($day_remains >= 0) ? $day_remains : 0;

                if(!$this->group()->month){
                    return $day_remains;
                }
            }
        }

        return min($day_remains, $month_remains);
    }

    public function isAdmin() {
        if($this->group() && $this->getGroupId() == slim_env('ADMIN_GROUP_ID', 1)){
            return true;
        }

        return false;
    }

    public function user() {
        return ($this->user instanceof User) ? $this->user : null;
    }

    public function group() {
        return $this->isLogged() ? $this->user()->group : null;
    }
}