<?php 
namespace App\Helpers\Olshansky\Pagination;

class Driver {
    private $view;
    private $template = 'pagination.twig';
    private $data;

    public function __construct(\Slim\Views\Twig $view)
    {
        $this->view = $view;
    }

    public function make(string $template, $data = null)
    {
        $this->data = $data;

        return $this;
    }

    public function __toString()
    {
        return $this->view->fetch($this->template, $this->data);
    }
}