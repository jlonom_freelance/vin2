<?php 
namespace App\Helpers\Olshansky\Session;

class Driver {
    protected $session_id;
    protected $path;
    public $data = [];

    public function __construct($container) {      
        $settings = $container->get('settings');
        
        $this->path = $settings['session']['path'];

        register_shutdown_function(array($this, 'close'));
    }
      
    public function getId() {
        return $this->session_id;
    }
 
    public function start($session_id = '') {
        if (!$session_id) {
            if (function_exists('random_bytes')) {
                $session_id = substr(bin2hex(random_bytes(26)), 0, 26);
            } else {
                $session_id = substr(bin2hex(openssl_random_pseudo_bytes(26)), 0, 26);
            }
        }

        if (preg_match('/^[a-zA-Z0-9,\-]{22,52}$/', $session_id)) {
            $this->session_id = $session_id;
        } else {
            exit('Error: Invalid session ID!');
        }
        
        $this->data = $this->read($session_id);
        
        return $session_id;
    }

    public function read($session_id) {
        $file = $this->path . 'sess_' . basename($session_id);

        if (is_file($file)) {
            $handle = fopen($file, 'r');

            flock($handle, LOCK_SH);

            $data = fread($handle, filesize($file));

            flock($handle, LOCK_UN);

            fclose($handle);

            return unserialize($data);
        } else {
            return array();
        }
    }
    
    public function close() {
        $this->write($this->session_id, $this->data);
    }

    public function write($session_id, $data) {
        $file = $this->path . 'sess_' . basename($session_id);

        $handle = fopen($file, 'w');

        flock($handle, LOCK_EX);

        fwrite($handle, serialize($data));

        fflush($handle);

        flock($handle, LOCK_UN);

        fclose($handle);

        return true;
    }
     
    public function __destroy() {
        $this->destroy($this->session_id);
    }

    public function destroy($session_id) {
        $file = $this->path . 'sess_' . basename($session_id);

        if (is_file($file)) {
            unset($file);
        }
    }

    public function __destruct() {
        if (ini_get('session.gc_divisor')) {
            $gc_divisor = ini_get('session.gc_divisor');
        } else {
            $gc_divisor = 1;
        }

        if (ini_get('session.gc_probability')) {
            $gc_probability = ini_get('session.gc_probability');
        } else {
            $gc_probability = 1;
        }

        $gc_divisor = 1000;
        $gc_probability = 1;

        if ((rand() % $gc_divisor) < $gc_probability) {
            $expire = time() - ini_get('session.gc_maxlifetime');

            $files = glob($this->path . 'sess_*');

            foreach ($files as $file) {
                if (filemtime($file) < $expire) {
                    unlink($file);
                }
            }
        }
    }
}