<?php
namespace App\Helpers\Google\ReCaptcha;

class Client {
    const SITE_VERIFY_HOSTNAME = 'https://www.google.com';

    const SITE_VERIFY_PATH = '/recaptcha/api/siteverify';

    private $private;

    private $public;

    public function __construct($container)
    {
        $settings = $container->get('settings');

        if(!empty($settings['google']['recaptcha_private']) && !empty($settings['google']['recaptcha_public'])){
            $this->private = $settings['google']['recaptcha_private'];
            $this->public = $settings['google']['recaptcha_public'];
        }
    }

    public function getPublic()
    {
        return $this->public ? $this->public : false;
    }

    public function verify($response = '')
    {
        if($this->private){
            if(empty($response)){
                return false;
            }

            $client = new \GuzzleHttp\Client([
                'base_uri' => self::SITE_VERIFY_HOSTNAME,
                'timeout'  => 10.0,
            ]);

            $google = $client->request('GET', self::SITE_VERIFY_PATH, [
                'query' => [
                    'secret' => $this->private,
                    'response' => $response,
                ],
            ]);

            $json = json_decode((string)$google->getBody(), true);
            
            if(isset($json['success']) && $json['success'] == true){
                return true;
            }
        }

        return false;
    }
}