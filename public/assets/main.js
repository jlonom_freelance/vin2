jQuery(document).ready(function($){
    let body = $('body');

    const serialize = function(obj, prefix) {
        var str = [],
            p;
        for (p in obj) {
            if (obj.hasOwnProperty(p)) {
                var k = prefix ? prefix + "[" + p + "]" : p,
                    v = obj[p];
                str.push((v !== null && typeof v === "object") ?
                    serialize(v, k) :
                    encodeURIComponent(k) + "=" + encodeURIComponent(v));
            }
        }
        return str.join("&");
    };

    body.on('click', '#order-search', function(event) {
        event.preventDefault();

        let searchId = $('#searchId').val();
        let searchStatus = $('#searchStatus option:selected').val();
        let searchSum = $('#searchSum').val();
        let searchName = $('#searchName').val();
        let searchVin = $('#searchVin').val();
        let searchDate = $('#searchDate').val();

        let queryData = {};
        if (searchId !== "") { queryData['searchId'] = searchId; }
        if (searchStatus !== "") { queryData['searchStatus'] = searchStatus; }
        if (searchSum !== "") { queryData['searchSum'] = searchSum; }
        if (searchName !== "") { queryData['searchName'] = searchName; }
        if (searchVin !== "") { queryData['searchVin'] = searchVin; }
        if (searchDate !== "") { queryData['searchDate'] = searchDate; }

        let queryString = serialize(queryData);

        window.location = '/order?' + queryString;
    });

    body.on('click', '#statistic-search-dates', function(event) {
        event.preventDefault();
        let dateFrom = $('#dateFrom').val();
        let dateTo = $('#dateTo').val();

        let queryData = {};
        if (dateFrom !== "") { queryData['dateFrom'] = dateFrom; }
        if (dateTo !== "") { queryData['dateTo'] = dateTo; }

        let queryString = serialize(queryData);

        window.location = '/statistics?' + queryString;
    });

    const sorting = document.querySelector('#selectUser');
    const sortingchoices = new Choices(sorting, {
        placeholder: "Введите имя пользователя",
        itemSelectText: ''
    });
});