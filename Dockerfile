FROM php:8.0.8-apache

ARG UNAME=appuser
ARG UID=1000
ARG GID=1000


RUN apt-get update && apt-get install -y \
    curl \
    g++ \
    git \
    libbz2-dev \
    libfreetype6-dev \
    libicu-dev \
    libjpeg-dev \
    libonig-dev\
    libmcrypt-dev \
    libpng-dev \
    libzip-dev \
    libreadline-dev \
    libxml2-dev \
    vim \
    sudo \
    unzip \
    zip \
 && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install \
    bcmath \
    bz2 \
    calendar \
    iconv \
    intl \
    gd \
    mbstring \
    opcache \
    pdo_mysql \
    mysqli \
    xml \
    zip

RUN pecl install -o -f redis \
    xdebug \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis \
    && { \
        echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)"; \
        echo "xdebug.mode=debug,develop"; \
        echo "xdebug.start_with_request=yes"; \
        echo "xdebug.client_host=host.docker.internal"; \
        echo "xdebug.client_port=9003"; \
        echo "xdebug.log_level=0"; \
    } > /usr/local/etc/php/conf.d/xdebug.ini;

WORKDIR /var/www/html

RUN a2enmod rewrite

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN groupadd -g $GID -o $UNAME
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME

USER $UNAME

EXPOSE 80/tcp
