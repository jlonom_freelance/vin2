<?php
use Dotenv\Dotenv;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Handlers\Critical;
use App\Handlers\NotFound;

$dotenv = Dotenv::createUnsafeImmutable(dirname(__DIR__));
$dotenv->load();

return [
    'settings' => [
        'displayErrorDetails' => slim_env('APP_DEBUG') ? true : false,
        'addContentLengthHeader' => false, 
        'determineRouteBeforeAppMiddleware' => true,
        'base_path' => slim_env('APP_URL'),
        'logger' => [
            'name' => 'slim-app',
            'path' => dirname(__DIR__) . '/storage/logs/app.log',
            'level' => slim_env('LOG_LEVEL'),
        ], 
        'db' => [
            'driver' => slim_env('DB_DRIVER'),
            'host' => slim_env('DB_HOST'),
            'port' => slim_env('DB_PORT'),
            'database' => slim_env('DB_DATABASE'),
            'username' => slim_env('DB_USERNAME'),
            'password' => slim_env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
        ],
        'view' => [
            'cache' => slim_env('APP_DEBUG') ? false : (dirname(__DIR__) . '/storage/cache/twig/'),
            'path' => dirname(__DIR__) . '/resources/views/',
        ], 
        'cli' => [
            'commands' => [
                'order/queue',
                'files/remove-old-files'
            ],
        ],
        'session' => [
            'path' => dirname(__DIR__) . '/storage/session/',
            'name' => 'SLIM_SESSION',
        ],
        'payment' => [
            'language' => 'uk',
            'currency' => 'UAH',
            'public' => slim_env('PAYMENT_PUBLIC'),
            'private' => slim_env('PAYMENT_PRIVATE'),
        ],
        'vin_api' => [
            'key' => slim_env('VIN_API_KEY'),
            'path' => dirname(__DIR__) . '/storage/downloads/',
        ],
        'telegram' => [
            'key' => slim_env('TELEGRAM_BOT_KEY'),
        ],
        'google' => [
            'recaptcha_private' => slim_env('GOOGLE_CAPTCHA_V2_PRIVATE'),
            'recaptcha_public' => slim_env('GOOGLE_CAPTCHA_V2_PUBLIC'),
        ],
    ],
];