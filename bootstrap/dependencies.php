<?php
use App\Extensions\TwigExtension;
use App\Helpers\Olshansky\Auth\Driver;
use App\Helpers\Olshansky\VINApi\Client;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Pagination\Paginator;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Slim\App;
use Slim\Container;
use Slim\Flash\Messages;
use Slim\Http\Uri;
use Slim\Views\Twig;

return function (App $app) {
    $container = $app->getContainer();

    $settings = $container->get('settings');
    $router = $container->get('router');

    $router->setBasePath($settings['base_path']);

    $container['logger'] = function (Container $container) use ($settings) {
        $logger = new Logger($settings['logger']['name']);

        $logger->pushProcessor(new UidProcessor());
        $logger->pushHandler(new StreamHandler($settings['logger']['path'], $settings['logger']['level']));

        return $logger;
    };

    $container['view'] = function ($container) use ($settings, $router) {
        $request = $container->get('request');

        $view = new Twig($settings['view']['path'], [
            'cache' => $settings['view']['cache'],
        ]);

        $router = $container->get('router');
        $uri = Uri::createFromEnvironment(new \Slim\Http\Environment($request->getServerParams()));
        $view->addExtension(new TwigExtension($router, $uri));

        $environment = $view->getEnvironment();
        $environment->addGlobal('is_logged', $container->get('auth')->isLogged());
        $environment->addGlobal('is_admin', $container->get('auth')->isAdmin());
        $environment->addGlobal('auth_id', $container->get('auth')->getId());
        $environment->addGlobal('auth_name', $container->get('auth')->getName());
        $environment->addGlobal('auth_group_id', $container->get('auth')->getGroupId());
        $environment->addGlobal('auth_price', $container->get('auth')->getPrice());
        $environment->addGlobal('auth_balance', $container->get('auth')->getBalance());
        $environment->addGlobal('auth_quota', $container->get('auth')->getQuota());
        $environment->addGlobal('recaptcha_public', $container->get('recaptcha')->getPublic());
        $environment->addGlobal('terms_link', $router->urlFor('page.public', ['slug' => 'terms']));
        $environment->addGlobal('condition_link', $router->urlFor('page.public', ['slug' => 'conditions']));
        return $view;
        
    };

    $container['db'] = function ($container) use ($settings) {
        $capsule = new Manager;

        $capsule->addConnection([
            'driver'        => $settings['db']['driver'],
            'host'          => $settings['db']['host'],
            'port'          => $settings['db']['port'],
            'database'      => $settings['db']['database'],
            'username'      => $settings['db']['username'],
            'password'      => $settings['db']['password'],
            'charset'       => $settings['db']['charset'],
            'collation'     => $settings['db']['collation'],
            'prefix'        => $settings['db']['prefix'],
        ]);

        return $capsule;
    };

    $container['flash'] = function ($container) {
        $session = $container->get('session');

        return new Messages($session->data);
    };

    $container['session'] = function ($container) {
        $session = new \App\Helpers\Olshansky\Session\Driver($container);

        return $session;
    };

    $container['recaptcha'] = function ($container) {
        $client = new \App\Helpers\Google\ReCaptcha\Client($container);

        return $client;
    };

    $container['vin_api'] = function ($container) {
        $client = new Client($container);

        return $client;
    };

    $db = $container->get('db');

    $db->setAsGlobal();
    $db->bootEloquent();

    $container['auth'] = function ($container) {
        $auth = new Driver($container);

        return $auth;
    };

    /*
        предотвращаем ненужную для CLI блокировку \Slim\Http\Environment в контенере. 
        Environment блокируется если запросить \Slim\Http\Request из контейнера
    */

    if (PHP_SAPI != 'cli') {
        $session = $container->get('session');
        $request = $container->get('request');

        Paginator::viewFactoryResolver(function () use ($container)  {
            $view = $container->get('view');

            return new \App\Helpers\Olshansky\Pagination\Driver($view);
        });

        Paginator::currentPathResolver(function () use ($request)  {
            return $request->getUri()->getPath();
        });


        Paginator::currentPageResolver(function ($pageName = 'page') use ($request) {

            $page = $request->getQueryParam($pageName);                                                                                                                                                              

            if (filter_var($page, FILTER_VALIDATE_INT) !== false && (int) $page >= 1) {                                                                                                                                    
                return $page;                                                                                                                                                                                              
            }                                                                                                                                                                                                         
            return 1;                                                                                                                                                                                                      
        });

        Paginator::queryStringResolver(function () use ($request)  {
            return $request->getUri()->getQuery();
        });

        if ($request->getCookieParam($settings['session']['name'])) {
            $session->start($request->getCookieParam($settings['session']['name']));
        }else{
            $session->start();
        }

        setcookie($settings['session']['name'], $session->getId(), ini_get('session.cookie_lifetime'), ini_get('session.cookie_path'), ini_get('session.cookie_domain'));
    }
};