<?php

use Slim\App;
use App\Http\Middleware\TrailingSlash;

return function (App $app) {
    $app->add(new TrailingSlash);
};