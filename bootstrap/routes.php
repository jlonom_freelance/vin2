<?php
use App\Http\Controllers\{FilesController,
    HomeController,
    AuthController,
    OrderController,
    PageController,
    StatisticsController,
    UserController,
    GroupController,
    TelegramController,
    TransactionController};
use App\Http\Middleware\{
    Authenticate,
    Authorization
};
use Slim\App;

return function (App $app) {
	$app->get('/', HomeController::class . ':index')->setName('home');

    $app->group('/auth', function (App $app) {
        $app->map(['GET', 'POST'], '/login', AuthController::class . ':login')->setName('auth.login');

        $app->get('/info', AuthController::class . ':info')->setName('auth.info');

        $app->map(['GET', 'POST'], '/edit', AuthController::class . ':edit')->setName('auth.edit')
            ->add(new Authenticate($app->getContainer()));

        $app->map(['GET', 'POST'], '/register', AuthController::class . ':register')->setName('auth.register');

        $app->get('/telegram', AuthController::class . ':telegram')->setName('auth.telegram')
            ->add(new Authenticate($app->getContainer()));
        $app->get('/telegram/disconnect', AuthController::class . ':telegramDisconnect')->setName('auth.telegram.disconnect')
            ->add(new Authenticate($app->getContainer()));

        $app->get('/logout', AuthController::class . ':logout')->setName('auth.logout');
    });

    $app->group('/user', function (App $app) {
        $app->get('/confirm-email/{token}', UserController::class . ':confirmEmail')->setName('user.confirmEmail');
    });

    $app->group('/page', function (App $app) {
        $app->get('', PageController::class . ':list')->setName('page.list');

        $app->map(['GET', 'POST'], '/add', PageController::class . ':store')->setName('page.add');

        $app->map(['GET', 'POST'], '/{id:[0-9]+}/edit', PageController::class . ':update')->setName('page.edit');

        $app->map(['GET'], '/{id:[0-9]+}/info', PageController::class . ':delete')->setName('page.delete');
    })
        ->add(new Authorization($app->getContainer()))
        ->add(new Authenticate($app->getContainer()));

    $app->group('/user', function (App $app) {
        $app->get('', UserController::class . ':list')->setName('user.list');

        $app->map(['GET', 'POST'], '/add', UserController::class . ':add')->setName('user.add');

        $app->map(['GET', 'POST'], '/{id:[0-9]+}/edit', UserController::class . ':edit')->setName('user.edit');

        $app->map(['GET', 'POST'], '/{id:[0-9]+}/info', UserController::class . ':info')->setName('user.info');
        $app->map(['GET'], '/{id:[0-9]+}/delete', UserController::class . ':delete')->setName('user.delete');
    })
        ->add(new Authorization($app->getContainer()))
        ->add(new Authenticate($app->getContainer()));

    $app->group('/group', function (App $app) {
        $app->get('', GroupController::class . ':list')->setName('group.list');

        $app->map(['GET', 'POST'], '/add', GroupController::class . ':add')->setName('group.add');

        $app->map(['GET', 'POST'], '/{id:[0-9]+}/edit', GroupController::class . ':edit')->setName('group.edit');
        $app->map(['GET'], '/{id:[0-9]+}/delete', GroupController::class . ':delete')->setName('group.delete');
    })
    ->add(new Authorization($app->getContainer()))
    ->add(new Authenticate($app->getContainer()));

    $app->group('/order', function (App $app) {

        $app->get('', OrderController::class . ':list')->setName('order.list');

        $app->map(['GET', 'POST'], '/create', OrderController::class . ':create')->setName('order.create');

        $app->get('/{id:[0-9]+}', OrderController::class . ':info')->setName('order.info');

        $app->get('/{id:[0-9]+}/download', OrderController::class . ':download')->setName('order.download');

        $app->get('/{id:[0-9]+}/payment', OrderController::class . ':payment')->setName('order.payment');

        $app->get('/{id:[0-9]+}/confirm', OrderController::class . ':confirm')->setName('order.confirm');

        $app->get('/{id:[0-9]+}/decline', OrderController::class . ':decline')->setName('order.decline');

        $app->map(['GET', 'POST'], '/{id:[0-9]+}/return', OrderController::class . ':return')->setName('order.return');
    })
    ->add(new Authenticate($app->getContainer()));

    $app->group('/order', function (App $app) {
        $app->map(['GET', 'POST'], '/webhook', OrderController::class . ':webhook')->setName('order.webhook');

        $app->map(['GET', 'POST'], '/queue', OrderController::class . ':queue')->setName('order.queue');
    });

    $app->group('/transaction', function (App $app) {
        $app->post('/create', TransactionController::class . ':create')->setName('transaction.create')
            ->add(new Authenticate($app->getContainer()));

        $app->get('/{id:[0-9]+}', TransactionController::class . ':info')->setName('transaction.info')
            ->add(new Authenticate($app->getContainer()));

        $app->get('/{id:[0-9]+}/check', TransactionController::class . ':check')->setName('transaction.check')
            ->add(new Authenticate($app->getContainer()));

        $app->map(['GET', 'POST'], '/{id:[0-9]+}/confirm', TransactionController::class . ':confirm')->setName('transaction.confirm')
            ->add(new Authenticate($app->getContainer()));    

        $app->map(['GET', 'POST'], '/{id:[0-9]+}/return', TransactionController::class . ':return')->setName('transaction.return');

        $app->map(['GET', 'POST'], '/{id:[0-9]+}/callback', TransactionController::class . ':callback')->setName('transaction.callback');
    });

    $app->get('/statistics', StatisticsController::class . ':index')->setName('statistics')
        ->add(new Authorization($app->getContainer()))
        ->add(new Authenticate($app->getContainer()));

    $app->group('/telegram', function (App $app) {
        $app->get('/{token:[0-9a-zA-Z]+}/download', TelegramController::class . ':download')->setName('telegram.download');

        $app->get('/set', TelegramController::class . ':setWebhook')->setName('telegram.set');

        $app->get('/delete', TelegramController::class . ':deleteWebhook')->setName('telegram.delete');

        $app->map(['GET', 'POST'], '/webhook', TelegramController::class . ':webhook')->setName('telegram.webhook');
    });

    $app->get('/{slug}', PageController::class . ':view')->setName('page.public');

    $app->group('/files', function (App $app) {
        $app->map(['GET', 'POST'], '/remove-old-files', FilesController::class . ':removeOldFiles')->setName('files.removeOld');
    });
};